from __future__ import division, print_function

import numpy as np
import os
import sklearn.pipeline
import sklearn.preprocessing
import tensorflow as tf
from sklearn.kernel_approximation import RBFSampler

import pdb

def get_variables(variables, name):
    for variable in variables:
        if variable.name.startswith(name):
            return variable
    return None

class Actor:
    """Estimates actions to perform given a state

    Actor generates a policy for an agent acting in a discrete environment. It
    uses a neural-network model to output the probability of 'n' actions given a
    particular state.
    """
    def __init__(self, sess, action_shape, input_shape, output_shape,
                 learning_rate=1e-4, scope='actor', summary_writer=None):
        """Initializes the Actor class

        Creates a TensorFlow graph of the Actor neural-network model. The model
        is capable of outputting multiple dimensions of actions to minimize the
        total the width of the output layer.

        Args:
            sess (:obj): A TensorFlow session.
            action_shape (:obj:`list` of int): Specifies size of action
                placeholder.
            input_shape (:obj:`list` of int): Specifies size of input state
                placeholder.
            output_shape (:obj:`list` of int): Specifies number of action
                outputs per each dimension.
            learning_rate (int, optional): Sets the learning rate for TensorFlow
                backpropogation.
            scope (str, optional): Scope under which TensorFlow graph will be initialized
                under.
        """
        self._sess = sess
        dimensions = len(output_shape)

        with tf.variable_scope(scope):
            self._state = tf.placeholder(tf.float32, input_shape, 'state')
            self._action = tf.placeholder(tf.int32, action_shape, 'action')
            self._target = tf.placeholder(dtype=tf.float32, name='target')

            # Create hidden layers
            self._layer1 = tf.layers.dense(
                tf.expand_dims(self._state, 0),
                256,
                activation=tf.nn.relu,
                name='layer1',
            )

            self._layer2 = tf.layers.dense(
                self._layer1,
                128,
                activation=tf.nn.relu,
                name='layer2',
            )

            self._layer3 = tf.layers.dense(
                self._layer2,
                64,
                activation=tf.nn.relu,
                name='layer3',
            )

            # Create Z output layer
            if dimensions >= 3:
                self._z_action_probs = tf.layers.dense(
                    self._layer3,
                    output_shape[-3],
                    activation=tf.nn.softmax,
                    kernel_initializer=tf.initializers.zeros,
                    name='z_action_probs',
                )
                self._z_action_probs = tf.squeeze(self._z_action_probs) + 1e-10
                self._z_action_prob = tf.gather(self._z_action_probs,
                                                self._action[-3])

                self._z_entropy = -tf.reduce_sum(self._z_action_probs * 
                                                 tf.log(self._z_action_probs))
                self._z_loss = -tf.log(self._z_action_prob) * self._target

            # Create Y output layer
            if dimensions >= 2:
                self._y_action_probs = tf.layers.dense(
                    self._layer3,
                    output_shape[-2],
                    activation=tf.nn.softmax,
                    kernel_initializer=tf.initializers.zeros,
                    name='y_action_probs',
                )
                self._y_action_probs = tf.squeeze(self._y_action_probs) + 1e-10
                self._y_action_prob = tf.gather(self._y_action_probs,
                                                self._action[-2])

                self._y_entropy = -tf.reduce_sum(self._y_action_probs * 
                                                 tf.log(self._y_action_probs))
                self._y_loss = -tf.log(self._y_action_prob) * self._target
 
            # Create X output layer
            if dimensions >= 1:
                self._x_action_probs = tf.layers.dense(
                    self._layer3,
                    output_shape[-1],
                    activation=tf.nn.softmax,
                    kernel_initializer=tf.initializers.zeros,
                    name='x_action_probs',
                )
                self._x_action_probs = tf.squeeze(self._x_action_probs) + 1e-10
                self._x_action_prob = tf.gather(self._x_action_probs,
                                                self._action[-1])

                self._x_entropy = -tf.reduce_sum(self._x_action_probs * 
                                                 tf.log(self._x_action_probs))
                self._x_loss = -tf.log(self._x_action_prob) * self._target
            
            # Merge output layers
            if dimensions == 3:
                self._action_probs = tf.concat([self._z_action_probs,
                                                self._y_action_probs,
                                                self._x_action_probs], 0)
                self._entropy = self._z_entropy + \
                                self._y_entropy + \
                                self._x_entropy
                self._loss = self._z_loss + self._y_loss + self._x_loss + \
                             0.01 * self._entropy

            elif dimensions == 2:
                self._action_probs = tf.concat([self._y_action_probs,
                                                self._x_action_probs], 0)
                self._entropy = self._y_entropy + self._x_entropy
                self._loss = self._y_loss + self._x_loss + 0.01 * self._entropy

            elif dimensions == 1:
                self._action_probs = self._x_action_probs
                self._entropy = self._x_entropy
                self._loss = self._x_loss + 0.01 * self._entropy
            
            # Loss and optimizer
            self._opt = tf.train.AdamOptimizer(learning_rate=learning_rate)
            self._grads_and_vars = self._opt.compute_gradients(self._loss)
            self._grads_and_vars = [[grad, var] for grad, var in
                                    self._grads_and_vars if grad is not None]
            self._train = self._opt.apply_gradients(self._grads_and_vars)

            # Summaries
            tf.summary.scalar('loss', self._loss)
            self._summary = tf.summary.merge_all() 

    def predict(self, state):
        return self._sess.run(self._action_probs, {self._state: state})

    def restore(self, dir):
        saver = tf.train.Saver()
        saver.restore(self._sess, os.path.join(dir, 'actor.ckpt'))

    def save(self, dir):
        saver = tf.train.Saver()
        saver.save(self._sess, os.path.join(dir, 'actor.ckpt'))

    def update(self, state, target, action):
        feed_dict = {
            self._state: state,
            self._target: target,
            self._action: action,
        }
        _, summary, loss = self._sess.run([self._train, self._summary, self._loss], feed_dict)
        return summary, loss

class Critic:
    def __init__(self, sess, input_shape, featurizer=None, learning_rate=0.001, scope='critic'):
        self._sess = sess
        self._featurizer = featurizer

        with tf.variable_scope(scope):
            self._state = tf.placeholder(tf.float32, input_shape, 'state')
            self._target = tf.placeholder(dtype=tf.float32, name='target')

            # Create hidden layer
            self._layer1 = tf.layers.dense(
                tf.expand_dims(self._state, 0),
                128,
                activation=tf.nn.relu,
                kernel_initializer=tf.initializers.zeros,
                name='layer1',
            )

            self._layer2 = tf.layers.dense(
                self._layer1,
                64,
                activation=tf.nn.relu,
                kernel_initializer=tf.initializers.zeros,
                name='layer2',
            )

            # Create value prediction layer
            self._value = tf.layers.dense(
                self._layer2,
                1,
                kernel_initializer=tf.initializers.zeros,
                name='value',
            )
            self._value = tf.squeeze(self._value)

            # Loss and optimizer
            self._loss = tf.squared_difference(self._value, self._target)
            self._opt = tf.train.AdamOptimizer(learning_rate=learning_rate)
            self._train = self._opt.minimize(self._loss)        

    def predict(self, state):
        return self._sess.run(self._value, {self._state: state})

    def update(self, state, target):
        feed_dict = {
            self._state: state,
            self._target: target
        }
        _, loss = self._sess.run([self._train, self._loss], feed_dict)
        return loss
