import matplotlib
import numpy as np
import pandas as pd
from collections import namedtuple
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from mpl_toolkits.mplot3d import Axes3D

matplotlib.style.use('ggplot')

EpisodeStats = namedtuple("Stats",["episode_lengths", "episode_rewards"])

def plot_value_function(V, title="Value Function"):
    """
    Plots the value function as a surface plot.
    """
    min_x = min(k[0] for k in V.keys())
    max_x = max(k[0] for k in V.keys())
    min_y = min(k[1] for k in V.keys())
    max_y = max(k[1] for k in V.keys())

    x_range = np.arange(min_x, max_x + 1)
    y_range = np.arange(min_y, max_y + 1)
    X, Y = np.meshgrid(x_range, y_range)

    # Find value for all (x, y) coordinates
    Z_noace = np.apply_along_axis(lambda _: V[(_[0], _[1], False)], 2, np.dstack([X, Y]))
    Z_ace = np.apply_along_axis(lambda _: V[(_[0], _[1], True)], 2, np.dstack([X, Y]))

    def plot_surface(X, Y, Z, title):
        fig = plt.figure(figsize=(20, 10))
        ax = fig.add_subplot(111, projection='3d')
        surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
                               cmap=matplotlib.cm.coolwarm, vmin=-1.0, vmax=1.0)
        ax.set_xlabel('Player Sum')
        ax.set_ylabel('Dealer Showing')
        ax.set_zlabel('Value')
        ax.set_title(title)
        ax.view_init(ax.elev, -120)
        fig.colorbar(surf)
        plt.show()

    plot_surface(X, Y, Z_noace, "{} (No Usable Ace)".format(title))
    plot_surface(X, Y, Z_ace, "{} (Usable Ace)".format(title))


def plot_action_probs(actor, env, goals, show=True, together=False,
                      plot_shape=None):
    dimensions = len(env._shape)
    if dimensions > 3:
        raise ValueError('Unable to handle environments greater than 3 dimensions')

    dims = ['z', 'y', 'x']

    num_actions = sum(env._shape)
    action_range = list(range(num_actions))
    action_half_range = np.array(action_range)[:-1] + 0.5
    action_labels = []
    for i, val in enumerate(env._shape):
        for j in range(val):
            label = dims[-dimensions + i] + str(j)
            action_labels.append(label)

    num_states = np.product(env._shape)
    state_range = list(range(num_states))
    state_half_range = np.array(state_range)[:-1] + 0.5
    states =  list(zip(*np.unravel_index(state_range, env._shape)))

    plots = []
    for goal in goals:
        data = []

        for start in states:
            state = env.reset(start, goals=[goal])
            action_probs = actor.predict(state)
            data.append(action_probs)

        plots.append((goal, data))

    # Plot plots together
    if together:
        if not plot_shape:
            return None
        
        # Create plots of 'plot_shape' as long as have data
        while len(plots) > 0:
            fig = plt.figure()
            grid = ImageGrid(fig, 111,
                             nrows_ncols=plot_shape,
                             cbar_mode='single',
                             cbar_location='right',
                             cbar_pad=0.1,
                             aspect=False,
                             axes_pad=0.25)

            for i, ax in enumerate(grid):
                if len(plots) <= 0:
                    continue

                (goal, data) = plots.pop(0)

                ax.set_title('Goal {}'.format(goal), fontsize=8)
                ax.set_xlabel('Actions')
                ax.set_ylabel('Start states')

                ax.set_xticks(action_range)
                ax.set_xticks(action_half_range, minor=True)
                ax.set_xticklabels(action_labels, fontsize=6)
                ax.xaxis.grid(False, which='major')
                ax.xaxis.grid(True, which='minor')

                ax.set_yticks(state_range)
                ax.set_yticks(state_half_range, minor=True)
                ax.set_yticklabels(states, fontsize=6)
                ax.yaxis.grid(False, which='major')
                ax.yaxis.grid(True, which='minor')

                im = ax.imshow(data, cmap='coolwarm', interpolation='none',
                               origin='lower')


            grid.cbar_axes[0].colorbar(im)
            if show:
                plt.show()
            else:
                plt.close()

    # Plot plots individually
    else:
        for (goal, data) in plots:
            fig, ax = plt.subplots(1)
            ax.set_title('Goal {}'.format(goal))
            ax.set_xlabel('Actions')
            ax.set_ylabel('Start states')

            ax.set_xticks(action_range)
            ax.set_xticks(action_half_range, minor=True)
            ax.set_xticklabels(action_labels, fontsize=6)
            ax.xaxis.grid(False, which='major')

            ax.set_yticks(state_range)
            ax.set_yticks(state_half_range, minor=True)
            ax.set_yticklabels(states, fontsize=6)
            ax.yaxis.grid(False, which='major')

            cax = ax.imshow(data, cmap='coolwarm', interpolation='none',
                            origin='lower')
            fig.colorbar(cax, ax=ax)

            if show:
                plt.show()
            else:
                plt.close()



def plot_episode_stats(stats, smoothing_window=10, show=False):
    # Plot the episode length over time
    fig1 = plt.figure(figsize=(10,5))
    plt.plot(stats.episode_lengths)
    plt.xlabel("Episode")
    plt.ylabel("Episode Length")
    plt.title("Episode Length over Time")
    if not show:
        plt.close(fig1)
    else:
        plt.show(fig1)

    # Plot the episode reward over time
    fig2 = plt.figure(figsize=(10,5))
    rewards_smoothed = pd.Series(stats.episode_rewards).rolling(smoothing_window, min_periods=smoothing_window).mean()
    plt.plot(rewards_smoothed)
    plt.xlabel("Episode")
    plt.ylabel("Episode Reward (Smoothed)")
    plt.title("Episode Reward over Time (Smoothed over window size {})".format(smoothing_window))
    if not show:
        plt.close(fig2)
    else:
        plt.show(fig2)

    # Plot time steps and episode number
    fig3 = plt.figure(figsize=(10,5))
    plt.plot(np.cumsum(stats.episode_lengths), np.arange(len(stats.episode_lengths)))
    plt.xlabel("Time Steps")
    plt.ylabel("Episode")
    plt.title("Episode per time step")
    if not show:
        plt.close(fig3)
    else:
        plt.show(fig3)

    return fig1, fig2, fig3
