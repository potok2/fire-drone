from collections import Iterable
import pickle
import time

def flatten(x):
    if isinstance(x, Iterable):
        return [el for sub in x for el in flatten(sub)]
    else:
        return [x]

def dump_verification_result(path):
    # Load pickle object
    with open(path, 'rb') as file:
        unsafe_decisions = pickle.load(file)

    # Print unsafe decisions
    print('Verification result:')
    states = sorted(unsafe_decisions.keys())
    for state in states:
        actions = sorted(unsafe_decisions[state])
        print('{}: {}'.format(state, actions))

    return unsafe_decisions

class Timer:
    def __enter__(self):
        self._start = time.clock()
        return self

    def __exit__(self, *args):
        self._end = time.clock()
        self.interval = self._end - self._start
