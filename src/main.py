from __future__ import division, print_function, absolute_import
from experiments.discrete.experiments import Env4x4, Env5x5, Env8x8
from lib.helper import dump_verification_result
import os
import pdb

# Global variables
EXP_DIR = '../experiments/combination/discrete'

def all_env_experiments(exp, which):
    if (which & 0x1) > 0:
        getattr(Env4x4, exp)()

    if (which & 0x2) > 0:
        getattr(Env8x8, exp)()

def env_experiment_all_verification(env, exp):
    ver_types = ['line', 'reach-tube', 'forward-sim']
    for ver_type in ver_types:
        path = os.path.join(EXP_DIR, env.env_name, exp, ver_type,
                            'verification.dat')
        dump_verification_result(path)
        print('')

def env_experiment_all_training(env, exp):
    ver_types = ['line', 'reach-tube', 'forward-sim']
    for ver_type in ver_types:
        getattr(env, exp)(ver_type)
        pdb.set_trace()

def main():
    #env_experiment_all_training(Env4x4, 'experiment3')
    #env_experiment_all_verification(Env4x4, 'experiment3')

    #Env4x4.experiment4('line')

    #Env4x4.experiment4_5('line')
    #Env4x4.experiment4_5('forward-sim')
    #Env4x4.experiment4_5('reach-tube')

    #Env4x4.experiment5('line')
    #Env4x4.experiment5('forward-sim')
    #Env4x4.experiment5('reach-tube')

    #Env4x4.experiment6('line')
    #Env4x4.experiment6('forward-sim')
    #Env4x4.experiment6('reach-tube')

    Env4x4.experiment7('line')
    #Env4x4.experiment7('forward-sim')
    #Env4x4.experiment7('reach-tube')

if __name__ == "__main__":
    main()
