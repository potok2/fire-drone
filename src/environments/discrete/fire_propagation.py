# -*- coding: utf-8 -*-
""" Fire propagation module.

This module implements a fire propagation model that propagates fire in the
environment grid as a 2D convolution between a 2D kernal, influence matrix, and
the environment. The influence matrix model an exogenous influence like the
wind.
"""

import itertools
import numpy as np

class FirePropagation:
    """Implements a particular fire propagation model."""
    def __init__(self, dims=2, influence=None, rate=1):
        if not 0 < dims < 4:
            err_str = 'Dims ({}) must be in [1, 3]'.format(dims)
            raise ValueError(err_str)

        self._dims = dims

        self._rate = rate
        self._counter = itertools.count()

        self._influence = np.ones(tuple([1 for dim in range(dims)]))
        if not influence is None:
            self._influence = influence
            if len(influence.shape) != dims:
                err_str = '''Influence dimension ({}) differs from dimensions
                             ({})'''.format(dims, len(influence.shape))
                raise ValueError(err_str)

        # Create a neighborhood of points to visit
        ranges = []
        for dim in influence.shape:
            ranges.append(list(range(-(dim//2), (dim//2)+1)))
        self._neighborhood = list(itertools.product(*ranges))

    def propagate(self, grid):
        # Return if no rate
        if self._rate == 0:
            return

        # Update every 'rate' steps
        time = next(self._counter)
        if (time % self._rate) > 0:
            return

        # Create a copy of the grid
        updated_grid = np.copy(grid)
        updated_grid[updated_grid < 0] = 0

        # Propogate the fire
        for coord in np.ndindex(*grid.shape):
            total_influence = 0
            for idx, dpos in enumerate(self._neighborhood):
                new_coord = tuple(np.add(coord, dpos))
                if not self._check_bounds(new_coord, grid.shape):
                    continue

                inf_coord = np.unravel_index(idx, self._influence.shape)
                if self._influence[inf_coord] > 0.0 and grid[new_coord] > 0.0:
                    total_influence += 1

            updated_grid[coord] = grid[coord] + total_influence

        # Limit the fire
        updated_grid[updated_grid > 4] = 4
        mask = grid >= 0
        np.copyto(grid, updated_grid, where=mask)

    def reset(self):
        self._counter = itertools.count()

    @staticmethod
    def _check_bounds(coord, shape):
        if (np.all(np.greater_equal(coord, (0, 0))) and
                np.all(np.less(coord, shape))):
            return True
        return False

