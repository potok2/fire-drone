# -*- coding: utf-8 -*-
"""Environment hyperplane module.

This module implements an n-dimensional grid-like environment. It supports two
types of modes: 'goal' and 'fire'. In 'goal' mode, the agent simply has to
reach a set of goals while in 'fire' mode, the agent has to put out cells that
are on fire.
"""
import numpy as np
from enum import IntEnum
from collections import defaultdict
from gym.envs.classic_control import rendering

import pdb

class HyperPlane:
    """An n-dimensional grid-like environment"""
    def __init__(self, agent, reward_funcs=None, water_reward_func=None,
                 fire_reward_func=None, shape=None, mode='fire',
                 fire_propagation=None, unsafe_decisions=None):
        """Constructs the HyperPlane environment

        Args:
            agent (obj): the agent that will interact with the environment
            reward_funcs (list of func): a list of functions for 'goal' mode
                                         where each function corresponds to a
                                         goal
            water_reward_func (func): reward function for obtaining water
            fire_reward_func (func): reward function for putting out a fire
            shape (list of ints): n-dimensional shape of the environment
            mode (str): environment mode from [goal, fire]
            fire_propagation (obj): fire propagation model
            unsafe_decisions (list of coords): action coordinates that are
                                               unsafe
        """
        self._agent = agent
        self._mode = mode
        self._shape = shape
        self._unsafe_decisions = unsafe_decisions
        if unsafe_decisions is None:
            self._unsafe_decisions = defaultdict(set)

        self._hyper_plane = None
        self._reward_time = None
        self._total_time = None
        self._viewer = None

        self._obstacles = None

        if mode == 'fire':
            self._water_reward_func = water_reward_func
            self._fire_reward_func = fire_reward_func

            self._water_reservoirs = None
            self._fire_locations = None

            self._fire_propagation = fire_propagation

            self._visited = None
            
        elif mode == 'goal':
            self._reward_funcs = reward_funcs
            self._goals = None

        # Cell states
        self._state = IntEnum('State', {
            'OBSTACLE': -3, 'WATER': -2, 'EXTINGUISHED': -1, 'UNLIT': 0,
            'FIRE1': 1, 'FIRE2': 2, 'FIRE3': 3, 'FIRE4': 4,
        })

    def reset(self, start=None, goals=None, water_reservoirs=None,
              fire_locations=None, obstacles=None):
        """Resets the HyperPlane environment

        Args:
            start (coord): coordinate where drone should start
            goals (list of coords): coordinates of goals in environment
            water_reservoirs (list of coords): coordinates of water reseirvoirs
                                               in environment
            fire_locations (list of coords): list of cells on fire in
                                             environment
            obstacles (list of coords): list of obstacles in environment
        """
        self._hyper_plane = np.zeros(self._shape, dtype=np.int32)

        # Handle fire mode
        if self._mode == 'fire':
            self._water_reservoirs = water_reservoirs
            for water_reservoir in water_reservoirs:
                self._hyper_plane[water_reservoir] = self._state.WATER

            self._fire_locations = fire_locations
            for fire_location in fire_locations:
                self._hyper_plane[fire_location] = self._state.FIRE4

            self._agent.reset(start, water_level=0)
            self._visited = set()
            if self._fire_propagation:
                self._fire_propagation.reset()

        # Handle goal mode
        elif self._mode == 'goal':
            self._goals = goals
            for i, goal in enumerate(goals):
                self._hyper_plane[goal] = i + 1

            self._agent.reset(start, cur_goal=1)

        # Add obstacles to environment
        self._obstacles = obstacles
        if not obstacles:
            self._obstacles = []
        for obstacle in self._obstacles:
            self._hyper_plane[obstacle] = self._state.OBSTACLE

        self._reward_time = 0
        self._total_time = 0
        return np.append(self._hyper_plane.flatten(), self._agent.state)

    def step(self, action, render=False, safe=True):
        """Updates environment a single step forward

        Args:
            action (coord): proposed action by the agent
            render (bool): flag to render the environment
            safe (bool): flag to determine whether the action is safe
        """
        cur_pos = self._agent.get_position()
        traj_safe = True

        # Check if action is an unsafe decision
        if (cur_pos in self._unsafe_decisions and
                action in self._unsafe_decisions[cur_pos]):
            safe = False

        # Handle no movement
        if cur_pos == action or action in self._obstacles:
            safe = False

        # Execute action if it is safe
        if safe:
            # Verify actual trajectory to see if safe
            traj_safe = self._agent.verify(action, self._shape,
                                           self._obstacles, 'forward-sim')
            ''' FIXME
            traj_safe = True
            '''

            # Move agent
            if self._agent.name == 'drone':
                render = self.render if render else None
                self._agent.move_position(action, render)
            else:
                self._agent.move_position(action)
            
            # Handle fire mode
            #elif self._mode == 'fire':
            if self._mode == 'fire':
                done, reward = True, -5.0
                cur_water_level = self._agent.water_level

                # Propagate the fire
                if not self._fire_propagation is None:
                    self._fire_propagation.propagate(self._hyper_plane)

                # Fill water
                if (self._hyper_plane[action] == self._state.WATER and
                        cur_water_level == 0):
                    self._agent.fill_water()
                    reward = self._water_reward_func(self._reward_time)
                    self._reward_time = -1

                    self._visited = set()

                # Release water
                elif self._hyper_plane[action] > 0 and cur_water_level > 0:
                    amount = self._agent.release_water(self._hyper_plane[action])
                    self._hyper_plane[action] -= amount

                    reward = self._fire_reward_func(self._hyper_plane[action])

                    if self._hyper_plane[action] == 0:
                        self._hyper_plane[action] = self._state.EXTINGUISHED

                    self._reward_time = -1
                    self._visited = set()

                else:
                    if action not in self._visited:
                        self._visited.add(action)
                    else:
                        reward = -1000000.0

                # Check if all cells are extinguished
                for cell in self._hyper_plane.flat:
                    if cell > 0:
                        done = False
                        reward -= (cell * 5.0 + 50.0)

            # Handle goal mode
            elif self._mode == 'goal':
                done, reward = False, -2.0
                cur_goal = self._agent.current_goal

                # Reward agent for reaching goal
                if self._hyper_plane[action] == cur_goal:
                    reward = self._reward_funcs[cur_goal-1](self._reward_time)
                    self._agent.increment_cur_goal()
                    self._reward_time = -1

                    # Check if episode completed
                    if cur_goal == len(self._goals):
                        done = True

        # Ignore action if not safe or not valid
        else:
            done, reward = False, -1000000.0
            next_state = cur_pos
        
        # Common mode actions
        self._reward_time += 1
        self._total_time += 1
        next_state = np.append(self._hyper_plane.flatten(), self._agent.state)
        return next_state, reward, done, safe, traj_safe

    def render(self, goal=None):
        """Renders the environment in a nice interface

        Note: this only works for 2D environments

        Args:
            goal (coord): coordinate where drone will move to
        """
        (ys, xs) = self._hyper_plane.shape

        grid_x, grid_y = 400, 400
        screen_size = (grid_x, grid_y)
        scale_x, scale_y = grid_x / xs, grid_y / ys

        def _transform(idx):
            (y, x) = idx

            trans_x = (x + 0.5) * scale_x
            trans_y = (ys - y - 0.5) * scale_y

            return (trans_x, trans_y)

        # Create viewer
        if self._viewer == None:
            self._viewer = rendering.Viewer(*screen_size)

            # Create grid
            for y in range(ys + 1):
                start = (0, y * scale_y)
                end = (grid_x, y * scale_y)
                line = rendering.make_polyline((start, end))
                self._viewer.add_geom(line)

            for x in range(xs + 1):
                start = (x * scale_x, 0)
                end = (x * scale_x, grid_y)
                line = rendering.make_polyline((start, end))
                self._viewer.add_geom(line)
        
            # Create drone
            drone_dim, half_drone_dim = 10, 5
            l, r, t, b = -half_drone_dim, half_drone_dim, -half_drone_dim, half_drone_dim
            drone = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            self._drone_trans = rendering.Transform()
            drone.add_attr(self._drone_trans)
            self._viewer.add_geom(drone)

        # Draw cell states
        for y in range(ys):
            for x in range(xs):
                cell = self._hyper_plane[y, x]
                if cell == self._state.WATER:
                    loc = rendering.Transform(translation=_transform((y, x)))
                    cell = rendering.make_circle(radius=10)
                    cell.add_attr(loc)
                    cell.set_color(0.0, 0.0, 1.0)
                    self._viewer.add_onetime(cell)

                elif cell == self._state.OBSTACLE:
                    loc = rendering.Transform(translation=_transform((y, x)))
                    cell = rendering.make_circle(radius=10)
                    cell.add_attr(loc)
                    cell.set_color(0.0, 0.0, 0.0)
                    self._viewer.add_onetime(cell)

                elif cell > 0:
                    loc = rendering.Transform(translation=_transform((y, x)))
                    cell = rendering.make_circle(radius=cell*2)
                    cell.add_attr(loc)
                    cell.set_color(1.0, 0.0, 0.0)
                    self._viewer.add_onetime(cell)

        # Draw drone
        drone_state = self._agent.get_state()
        drone_state = _transform(drone_state[:2][::-1])
        self._drone_trans.set_translation(*drone_state)

        # Draw flag
        if goal:
            trans_goal = _transform(goal)
            (flag_x, flag_ybot) = trans_goal
            (_, flag_ytop) = _transform(goal - np.array((0.5, 0.0)))

            flagpole = rendering.Line((flag_x, flag_ybot),
                                      (flag_x, flag_ytop))
            flagpole.set_color(0.0, 0.8, 0.0)
            self._viewer.add_onetime(flagpole)

            flag = rendering.FilledPolygon([(flag_x, flag_ytop), 
                                            (flag_x, flag_ytop - 0.25 * scale_y),
                                            (flag_x + 0.25 * scale_x, flag_ytop - 0.125 * scale_y)])
            flag.set_color(0.8, 0.8, 0.0)
            self._viewer.add_onetime(flag)

        return self._viewer.render()

    def add_verification_decision(self, decision, safe):
        """Adds unsafe decisions to local storage

        Args:
            decision (coord): proposed action by agent
            safe (bool): safety of decision
        """
        if not safe:
            self._unsafe_decisions[decision[0]].add(decision[1])
