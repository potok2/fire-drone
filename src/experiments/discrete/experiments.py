# -*- coding: utf-8 -*-
"""Discrete experiment module.

This module implements a general Experiment class for the three versions of
(pre, peri, post)-verification training and two child classes for individual
environments (Env4x4, Env8x8). Each of the child classes has functions which run
an actual experiment.
"""

from __future__ import division, print_function

from collections import defaultdict, deque, namedtuple, OrderedDict
from glob import glob

from agents.discrete.hopper import Hopper
from agents.continuous.drone import Drone
from environments.discrete.hyper_plane import HyperPlane
from environments.discrete.fire_propagation import FirePropagation
from lib.helper import Timer
from policies.discrete.actor_critic import Actor, Critic

import itertools
import lib.plotting as plotting
import math as m
import numpy as np
import os
import pickle
import random
import sh
import tensorflow as tf
import time

import pdb

# Settings
np.seterr(all='raise')
np.set_printoptions(precision=6)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# Global variables
EXP_DIR = '../experiments/combination/discrete'

class Experiment:
    """Exposes training functions for training on general enviroments."""
    sess = tf.Session()

    @classmethod
    def pre_verification_training(cls, mode, train_agent, verify_agent,
                                  reset_func, reward_funcs, num_episodes,
                                  ver_type, actor_lr=1e-4, critic_lr=1e-3,
                                  save_agent_dir_path=None, save_ver_path=None,
                                  save_perf_path=None, reload=True,
                                  fire_propagation=None, discount_factor=1.0,
                                  cache_results=False):
        """Executes the pre-verification traininig algorithm.

        The pre-verification training algorthm first verifies an entire
        environment and stores the result in a user defined location. It then
        uses the verification results to train a policy.

        Args:
            mode (str): the objective type from the list [goal, fire]
            train_agent (obj): the agent used for training the policy
            verify_agent (obj): the agent used for verification
            reset_func (func): a function that specifies how to reset the
                               environemnt after each episodes
            reward_funcs (list of func): a list of two functions: reward
                                         function for obtaining water and reward
                                         for dumping water on a fire cell
            num_episodes (int): the number of episodes
            actor_lr (float): the actor's learning rate
            critic_lr (float): the critic's learning rate
            save_agent_dir_path (str): path of directory where to save policy
            save_ver_path (str): path where to save verification results
            reload (bool): use existing verification results if they exist
            fire_propagation (obj): model of fire_propagation
            discount_factor (float): weight of future rewards
        """
        # Pre-verify environment
        print('Pre-verify environment')
        verification_counter = 0 

        with Timer() as total_time:
            # Verify environment
            with Timer() as verify_timer:
                unsafe_decisions = defaultdict(set)
                reset_info = reset_func()

                if os.path.exists(save_ver_path) and reload:
                    verification_counter = np.product(cls.env_shape)
                    with open(save_ver_path, 'rb') as file:
                        unsafe_decisions = pickle.load(file)
                elif not 'obstacles' in reset_info:
                    pass
                else:
                    ranges = []
                    for dim in cls.env_shape:
                        ranges.append(list(range(dim)))

                    # Gather all possible decisions
                    coordinates = list(itertools.product(*ranges))
                    paths = list(itertools.product(coordinates, repeat=2))

                    # Verify paths
                    unsafe_decisions = defaultdict(set)
                    obstacles = reset_func()['obstacles']
                    for (state, action)  in paths:
                        verify_agent.set_position(state)
                        safe = verify_agent.verify(action, cls.env_shape,
                                                   obstacles, 'reach-tube')
                        if not safe:
                            unsafe_decisions[state].add(action)
                        verification_counter += 1

                    # Save unsafe decisions
                    if save_ver_path:
                        with open(save_ver_path, 'wb') as file:
                            pickle.dump(unsafe_decisions, file, pickle.HIGHEST_PROTOCOL)

            # Initialize variables
            train_env, verify_env, actor, critic = \
                cls._initialize_variables(mode, train_agent, verify_agent,
                                          reset_func, reward_funcs,
                                          actor_lr=actor_lr, critic_lr=critic_lr,
                                          fire_propagation=fire_propagation,
                                          unsafe_decisions=unsafe_decisions)

            # Train policy
            print('Train policy')
            performance = cls._train_policy(verify_env, verify_agent, actor, critic, reset_func,
                                            num_episodes, ver_type, save_agent_dir_path,
                                            show_plots=False)
            '''
            performance = cls._train_policy(train_env, train_agent, actor, critic, reset_func,
                                            num_episodes, ver_type, save_agent_dir_path,
                                            show_plots=False)
            '''

            # Modify performance variables
            performance = list(performance)
            performance[1:3] = (verify_timer.interval, verification_counter)
            for state in unsafe_decisions.keys():
                for action in unsafe_decisions[state]:
                    performance[3][(state, action)][0] += 1
            performance = tuple(performance)

        # Save performance statistics
        performance = (total_time.interval,) + performance
        cls.print_performance(performance, save_perf_path)

        # Execute policy
        print('Execute policy')
        pdb.set_trace()
        cls._execute_policy(verify_env, actor, reset_func)

    @classmethod
    def peri_verification_training(cls, mode, train_agent, verify_agent,
                                   reset_func, reward_funcs, num_episodes,
                                   ver_type, actor_lr=1e-4, critic_lr=1e-3,
                                   save_agent_dir_path=None, save_ver_path=None,
                                   save_perf_path=None, reload=True,
                                   fire_propagation=None, cache_results=True,
                                   discount_factor=1.0):
        """Executes the peri-verification traininig algorithm.

        The peri-verification training algorthm verifies the environment as it
        learns the policy. Whenever the algorithm encounters a (state, action)
        pair that has not been verified yet, it will verify said pair for
        safety. If an (state, action) pair has already been verified, the
        algorithm will used a previously cached result.
        
        Args:
            mode (str): the objective type from the list [goal, fire]
            train_agent (obj): the agent used for training the policy
            verify_agent (obj): the agent used for verification
            reset_func (func): a function that specifies how to reset the
                               environemnt after each episodes
            reward_funcs (list of func): a list of two functions: reward
                                         function for obtaining water and reward
                                         for dumping water on a fire cell
            num_episodes (int): the number of episodes
            actor_lr (float): the actor's learning rate
            critic_lr (float): the critic's learning rate
            save_agent_dir_path (str): path of directory where to save policy
            save_ver_path (str): path where to save verification results
            reload (bool): use existing verification results if they exist
            fire_propagation (obj): model of fire_propagation
            cache_results (bool): caches verification results for training
            discount_factor (float): weight of future rewards
        """
        # Peri-verify environment
        print('Peri-verify environment')
        with Timer() as total_time:
            # Initialize variables
            # FIXME
            train_env, verify_env, actor, critic = \
                cls._initialize_variables(mode, train_agent, verify_agent,
                                          reset_func, reward_funcs,
                                          actor_lr=actor_lr, critic_lr=critic_lr,
                                          fire_propagation=fire_propagation)

            # Train policy
            print('Train policy')
            performance = cls._train_policy(verify_env, verify_agent, actor, critic, reset_func,
                                            num_episodes, ver_type, save_agent_dir_path, show_plots=False,
                                            verify=True, cache_results=cache_results)

        # Save performance statistics
        performance = (total_time.interval,) + performance
        cls.print_performance(performance, save_perf_path)

        # Execute policy
        print('Execute policy')
        pdb.set_trace()
        cls._execute_policy(verify_env, actor, reset_func)

    @classmethod
    def post_verification_training(cls, mode, train_agent, verify_agent,
                                   reset_func, reward_funcs, num_episodes,
                                   ver_type, actor_lr=1e-4, critic_lr=1e-3,
                                   save_agent_dir_path=None, save_ver_path=None,
                                   reload=True, fire_propagation=None,
                                   discount_factor=1.0):
        """Executes the post-verification traininig algorithm.

        The post-verification training algorithm runs in a loop until a safe
        policy has been discovered and executes without the agent running into
        any unsafe states. Inside the loop, the algorithm learns and executes
        the learned policy. If the policy is unsafe, the unsafe action is cached
        and the entire training loop is repeated.

        Args:
            mode (str): the objective type from the list [goal, fire]
            train_agent (obj): the agent used for training the policy
            verify_agent (obj): the agent used for verification
            reset_func (func): a function that specifies how to reset the
                               environemnt after each episodes
            reward_funcs (list of func): a list of two functions: reward
                                         function for obtaining water and reward
                                         for dumping water on a fire cell
            num_episodes (int): the number of episodes
            actor_lr (float): the actor's learning rate
            critic_lr (float): the critic's learning rate
            save_agent_dir_path (str): path of directory where to save policy
            save_ver_path (str): path where to save verification results
            reload (bool): use existing verification results if they exist
            fire_propagation (obj): model of fire_propagation
            discount_factor (float): weight of future rewards
        """
        # Post-verify environment
        print('Post-verify environment')
        relearn_count = 0
        unsafe_decisions = defaultdict(set)
        while True:
            print('Iteration {}'.format(relearn_count))
            print(unsafe_decisions)

            # Initialize variables
            train_env, verify_env, actor, critic = \
                cls._initialize_variables(mode, train_agent, verify_agent,
                                          reset_func, reward_funcs,
                                          actor_lr=actor_lr, critic_lr=critic_lr,
                                          fire_propagation=fire_propagation,
                                          unsafe_decisions=unsafe_decisions)

            # Train policy
            performance = cls._train_policy(train_env, train_agent, actor, critic,
                                            reset_func, num_episodes, ver_type,
                                            save_agent_dir_path, show_plots=False)

            # Verify policy
            unsafe_decision = cls._verify_policy(verify_env, verify_agent,
                                                 reset_func, actor=actor,
                                                 render=True)
            if not unsafe_decision:
                break
            unsafe_decisions[unsafe_decision[0]].add(unsafe_decision[1])

            # Reste Tensorflow session
            relearn_count += 1
            tf.reset_default_graph()
            cls.sess = tf.Session()

        # Save policy
        if save_agent_dir_path:
            actor.save(save_agent_dir_path)

        # Execute policy
        print('Execute policy')
        pdb.set_trace()
        cls._execute_policy(verify_env, actor, reset_func)

    @classmethod
    def run_policy(cls, mode, train_agent, verify_agent, reset_func,
                   reward_funcs, save_dir_path):
        """Loads a previously saved agent and runs its learned policy

        Args: 
            mode (str): the objective type from the list [goal, fire]
            train_agent (obj): the agent used for training the policy
            verify_agent (obj): the agent used for verification
            reset_func (func): a function that specifies how to reset the
                               environemnt after each episodes
            reward_funcs (list of func): a list of two functions: reward
                                         function for obtaining water and reward
                                         for dumping water on a fire cell
            save_agent_dir_path (str): path of directory where to save policy
        """
        # Initialize variables
        _, env, actor, _ = \
            cls._initialize_variables(mode, train_agent, verify_agent,
                                      reset_func, reward_funcs)
        actor.restore(save_dir_path)

        # Execute policy
        cls._execute_policy(env, actor, reset_func)
        

    @classmethod
    def _execute_policy(cls, env, actor, reset_func):
        """Executes a policy

        Args:
            env (obj): the environment
            actor (obj): a policy
            reset_func (func): a function that sepecifes how to reset the
                               environment after each episode
        """
        Transition = namedtuple('Transition',
                                ['state', 'action', 'reward', 'next_state', 'done'])
        episode = []

        state_size = np.prod(cls.env_shape)
        state, done = env.reset(**reset_func()), False

        t = itertools.count()
        while not done:
            all_probs = actor.predict(state)
            action = cls._max_action(all_probs)
            next_state, reward, done, _, _ = env.step(action, render=True)

            # Update trace
            episode.append(Transition(state, action, reward, next_state, done))

            # Print step
            print('{})\nenvironment:\n{}\nagent: {}, action: {}\nreward: {}, done: {}'.format(next(t),
                  np.reshape(state[:state_size], cls.env_shape), state[state_size:],
                  action, reward, done))
            state = next_state

        # Print last step
        print('{})\nenvironment:\n{}\nagent: {}'.format(next(t),
              np.reshape(state[:state_size], cls.env_shape), state[state_size:]))

        return episode
    
    @classmethod
    def _initialize_variables(cls, mode, train_agent, verify_agent, reset_func,
                              reward_funcs, actor_lr=1e-4, critic_lr=1e-3,
                              fire_propagation=None, unsafe_decisions=None):
        """
        Args:
            mode (str): the objective type from the list [goal, fire]
            train_agent (obj): the agent used for training the policy
            verify_agent (obj): the agent used for verification
            reset_func (func): a function that specifies how to reset the
                               environemnt after each episodes
            num_episodes (int): the number of episodes
            actor_lr (float): the actor's learning rate
            critic_lr (float): the critic's learning rate
            save_agent_dir_path (str): path of directory where to save policy
            save_ver_path (str): path where to save verification results
            reload (bool): use existing verification results if they exist
            fire_propagation (obj): model of fire_propagation
            discount_factor (float): weight of future rewards
        """

        # Initialize environments
        if mode == 'goal':
            train_env = HyperPlane(train_agent, reward_funcs=reward_funcs,
                                   shape=cls.env_shape, mode=mode,
                                   unsafe_decisions=unsafe_decisions)
            verify_env = HyperPlane(verify_agent, reward_funcs=reward_funcs,
                                    shape=cls.env_shape, mode=mode,
                                    unsafe_decisions=unsafe_decisions)

        elif mode == 'fire':
            train_env = HyperPlane(train_agent, water_reward_func=reward_funcs[0],
                                   fire_reward_func=reward_funcs[1], shape=cls.env_shape,
                                   mode=mode, unsafe_decisions=unsafe_decisions,
                                   fire_propagation=fire_propagation)
            verify_env = HyperPlane(verify_agent, water_reward_func=reward_funcs[0],
                                    fire_reward_func=reward_funcs[1], shape=cls.env_shape,
                                    mode=mode, unsafe_decisions=unsafe_decisions,
                                    fire_propagation=fire_propagation)

        # Initialize policy
        tf.reset_default_graph()
        cls.sess = tf.Session()

        actor = Actor(cls.sess, cls.action_shape, cls.input_shape,
                      cls.env_shape, learning_rate=actor_lr)
        critic = Critic(cls.sess, cls.input_shape, learning_rate=critic_lr)

        return train_env, verify_env, actor, critic

    @classmethod
    def _max_action(cls, all_probs):
        """ Selects the action for each dimension with the maximum probability

        Args:
            all_probs (list): list of probabilities for each dimension
        """
        action = np.zeros(len(cls.env_shape), dtype=np.int32)

        lo, hi = 0, cls.env_shape[0]
        for i, dim in enumerate(cls.env_shape):
            action_probs = all_probs[lo:hi]
            action[i] = np.argmax(action_probs)
            lo, hi = hi, hi + dim

        return tuple(action)

    @classmethod
    def _select_action(cls, all_probs):
        """ Randomly selects an action for each dimension

        Args:
            all_probs (list): list of probabilities for each dimension
        """
        action = np.zeros(len(cls.env_shape), dtype=np.int32)

        lo, hi = 0, cls.env_shape[0]
        for i, dim in enumerate(cls.env_shape):
            action_probs = all_probs[lo:hi]
            action[i] = np.random.choice(len(action_probs), p=action_probs)
            lo, hi = hi, hi + dim

        return tuple(action)

    # FIXME show_plots is unused. Remove and return 'stats' instead
    @classmethod
    def _train_policy(cls, env, agent, actor, critic, reset_func, num_episodes,
                      ver_type, save_agent_dir_path=None, discount_factor=1.0,
                      show_plots=True, verify=False, cache_results=True,
                      modify_verify_state=True):
        """Trains a policy. 

        This function is used internally by the (pre, peri, post)-verification
        training algorithms.

        Args:
            env (obj): the environment to train against
            agent (obj): the agent to explore the environment
            actor (obj): the network for the Actor
            critic (obj): the network for the Critic
            reset_func (func): a function that specifies how to reset the
                               environemnt after each episodes
            num_episodes (int): the number of episodes
            ver_type (str): the type of verification (pre, peri, post)
            save_agent_dir_path (str): path of directory where to save policy
            discount_factor (float): weight of future rewards
            show_plots (bool): flag to display the training plots of NNs
            verify (bool): flag to specify whether each decision should be
                           verified for safety
            cache_results (bool): flag to cache verification results of each
                                  decision
            modify_verify_state (bool): flag to change the agent's state if the
                                        verification method uses quantization
        """
        # Performance variables
        train_time, verify_time = 0.0, 0.0
        verification_counter = 0

        ranges = []
        for dim in cls.env_shape:
            ranges.append(list(range(dim)))
        coordinates = list(itertools.product(*ranges))
        paths = list(itertools.product(coordinates, repeat=2))
        safety_counts = OrderedDict((path, [0, 0]) for path in paths)

        # Initialization
        cls.sess.run(tf.global_variables_initializer())

        # Stats
        stats = plotting.EpisodeStats(
            episode_lengths=np.zeros(num_episodes),
            episode_rewards=np.zeros(num_episodes),
        )

        # Iterate over episodes
        if verify and cache_results:
            safety_matrix = {}

        for i_episode in range(num_episodes):
            state = env.reset(**reset_func())

            for t in itertools.count():
                # Select action
                with Timer() as timer:
                    all_probs = actor.predict(state)
                    action = cls._select_action(all_probs)
                train_time += timer.interval

                # Verify action and update environment
                decision = (agent.get_position(), action)
                safe = True
                if verify:
                    with Timer() as timer:
                        # Modify agent state for quantization
                        if modify_verify_state:
                            pre_verify_state = agent.get_state()
                            agent.quantize_state(decision[0])

                        # Verify action
                        if cache_results and decision not in safety_matrix:
                            safe = agent.verify(action, cls.env_shape,
                                                env._obstacles, ver_type)
                            safety_matrix[decision] = safe
                            env.add_verification_decision(decision, safe)
                        elif not cache_results:
                            safe = agent.verify(action, cls.env_shape,
                                                env._obstacles, ver_type)

                        # Reset state to pre-verification
                        if modify_verify_state:
                            agent.set_state(pre_verify_state)

                    verification_counter += 1
                    if not safe:
                        safety_counts[decision][0] += 1
                    verify_time += timer.interval

                if t % 1000 == 0 and not t == 0:
                    #pdb.set_trace()
                    print(state, all_probs)

                # Break if agent stuck for a long time
                """
                if t == 5000:
                    break
                """

                # Update environment
                with Timer() as timer:
                    next_state, reward, done, safe, traj_safe = env.step(action, safe=safe)
                    if safe and not traj_safe:
                        safety_counts[decision][1] += 1

                    # Update statistics
                    stats.episode_lengths[i_episode] = t
                    stats.episode_rewards[i_episode] += reward
                    
                    # Update networks
                    value_next = critic.predict(next_state)
                    td_target = reward + discount_factor * value_next
                    td_error = td_target - critic.predict(state)

                    critic.update(state, td_target)
                    actor.update(state, td_error, action)

                    # Print information
                    """
                    print('\rStep {} @\tepisode {}/{} ({})'.format(
                        t, i_episode+1, num_episodes, stats.episode_rewards[i_episode-1]), end="")
                    """

                train_time += timer.interval

                if done:
                    '''
                    print('Step {} @\tepisode {}/{} ({})'.format(
                        t, i_episode+1, num_episodes, stats.episode_rewards[i_episode-1]))
                    '''
                    break
                
                state = next_state

            print('Step {} @\tepisode {}/{} ({})'.format(
                t, i_episode+1, num_episodes, stats.episode_rewards[i_episode-1]))


        # Save learned policy
        if save_agent_dir_path:
            actor.save(save_agent_dir_path)

        return train_time, verify_time, verification_counter, safety_counts

    @classmethod
    def _verify_policy(cls, env, agent, reset_func, actor=None, save_path=None, render=False):
        """ Verifies a policy for safety.

        Args:
            env (obj): the environment to train against
            agent (obj): the agent to explore the environment
            reset_func (func): a function that specifies how to reset the
                               environemnt after each episodes
            actor (obj): the network for the Actor
            save_path (str): path of directory where to save policy
            render (bool): flag to display the environment
        """
        # Restore policy
        if not actor:
            actor.restore(save_path)

        # Verify policy
        state_size = np.prod(cls.env_shape)
        state, done = env.reset(**reset_func()), False

        if render:
            env.render()

        t = itertools.count()
        while not done:
            all_probs = actor.predict(state)
            action = cls._max_action(all_probs)

            safety, reach = agent.verify(action, cls.env_shape, env._obstacles)
            if not safety == 'SAFE':
                pos = agent.get_position()
                return [pos, action]

            next_state, reward, done = env.step(action)

            print('{})\nenvironment:\n{}\nagent: {}, action: {}\nreward: {}, done: {}'.format(next(t),
                    np.reshape(state[:state_size], cls.env_shape), state[state_size:],
                    action, reward, done))
            print(agent.get_state())
            state = next_state

            if render:
                env.render(action)

        return None
    
    @staticmethod
    def make_directory(dir):
        if not os.path.exists(dir):
            os.makedirs(dir)
        elif not os.path.isdir(dir):
            raise ValueError('Path {} is not a directory'.format(dir))

    @staticmethod
    def print_performance(performance, save_path):
        if not len(performance) == 5:
            raise ValueError('Variable performance should have length 5')

        time_str = ('Total time: {}\n'
                    '--train time: {}\n'
                    '--verification time: {}\n')
        time_str = time_str.format(*performance[:3])

        count_str = 'Verification count: {}\n'.format(performance[3])
        unsafe_verification_counter, unsafe_trajectory_counter = 0, 0
        for key, value in performance[4].iteritems():
            count_str += '--{}: {}\n'.format(key, value)
            unsafe_verification_counter += value[0]
            unsafe_trajectory_counter += value[1]

        count_str += ('Unsafe verification count: {}\n'
                      'Unsafe trajectory count: {}\n').format(
                          unsafe_verification_counter,
                          unsafe_trajectory_counter)

        with open(save_path, 'w') as file:
            file.write(time_str + '\n' + count_str)


class Env4x4(Experiment):
    env_name = 'env4x4'
    env_shape = (4, 4)
    action_shape = (2,)
    input_shape = (19,)

    @classmethod
    def experiment1(cls, ver_type):
        """ Experiment1 environment
        ┏━┳━┳━┳━┓
        ┃S┃ ┃ ┃W┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━┫
        ┃F┃ ┃ ┃ ┃
        ┗━┻━┻━┻━┛
        """

        exp_name = 'experiment1'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        cls.make_directory(save_dir_path)
        
        # Functions
        water_reward_func = lambda x: 50.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 3),),
                'fire_locations': ((3, 0),),
            }

        # Run experiment
        num_episodes = 500
        reward_funcs = [water_reward_func, fire_reward_func]
        
        # Agents
        train_agent = Hopper(mode)
        verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                             ver_type=ver_type)

        if ver_type == 'line':
            verification_training = cls.peri_verification_training
            cache_results = False
        elif ver_type == 'forward-sim': 
            verification_training = cls.peri_verification_training
            cache_results = False
        elif ver_type == 'reach-tube':
            verification_training = cls.pre_verification_training
            cache_results = True
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Verify
        verification_training(mode, train_agent, verify_agent, reset_func,
                              reward_funcs, num_episodes,
                              save_agent_dir_path=save_dir_path,
                              save_ver_path=save_ver_path,
                              cache_results=cache_results)

        '''
        # Pre-verification
        cls.pre_verification_training(mode, train_agent, verify_agent, reset_func,
                                      reward_funcs, num_episodes,
                                      save_agent_dir_path=save_dir_path,
                                      save_ver_path=save_ver_path)
        '''

    @classmethod
    def experiment2(cls, ver_type):
        """ Experiment2 environment
        ┏━┳━┳━┳━┓
        ┃S┃ ┃ ┃W┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━┫
        ┃F┃ ┃ ┃ ┃
        ┗━┻━┻━┻━┛
        """

        exp_name = 'experiment2'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        cls.make_directory(save_dir_path)

        # Agents
        if ver_type == 'line':
            train_agent = Hopper(mode)
            verify_agent = Hopper(mode)
        elif ver_type == 'reach-tube':
            train_agent = Hopper(mode)
            verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode)
        elif ver_type == 'forward-sim':
            train_agent = Hopper(mode)
            verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                                 ver_type='forward-sim')
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Fire
        rate = 16
        influence = np.array(((0.0, 0.0, 0.0),
                              (1.0, 1.0, 0.0),
                              (0.0, 1.0, 0.0)))
        fire_propagation = FirePropagation(influence=influence, rate=rate)

        # Functions
        water_reward_func = lambda x: 50.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 3),),
                'fire_locations': ((3, 0),),
            }

        # Run experiment
        num_episodes = 5000
        reward_funcs = [water_reward_func, fire_reward_func]

        # Pre-verification
        cls.pre_verification_training(mode, train_agent, verify_agent, reset_func,
                                      reward_funcs, num_episodes,
                                      save_agent_dir_path=save_dir_path,
                                      save_ver_path=save_ver_path)



    @classmethod
    def experiment3(cls, ver_type):
        """ Experiment3 environment
        ┏━┳━┳━┳━┓
        ┃S┃ ┃U┃W┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━┫
        ┃F┃ ┃ ┃ ┃
        ┗━┻━┻━┻━┛
        """

        exp_name = 'experiment3'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        save_perf_path = os.path.join(save_dir_path, 'performance.txt')
        cls.make_directory(save_dir_path)

        # Functions
        water_reward_func = lambda x: 50.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 3),),
                'fire_locations': ((3, 0),),
                'obstacles': ((0, 2),),
            }

        # Run experiment
        num_episodes = 2500
        reward_funcs = [water_reward_func, fire_reward_func]

        # Agents
        train_agent = Hopper(mode)
        verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                             ver_type=ver_type)

        if ver_type == 'line':
            verification_training = cls.peri_verification_training
            cache_results = False
        elif ver_type == 'forward-sim': 
            verification_training = cls.peri_verification_training
            cache_results = False
        elif ver_type == 'reach-tube':
            verification_training = cls.pre_verification_training
            cache_results = True
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Verify
        performance = verification_training(mode, train_agent, verify_agent, reset_func,
                                            reward_funcs, num_episodes,
                                            save_agent_dir_path=save_dir_path,
                                            save_ver_path=save_ver_path,
                                            cache_results=cache_results)
        cls.print_performance(performance, save_perf_path)

    @classmethod
    def experiment4(cls, ver_type):
        """ Experiment4 environment
        ┏━┳━┳━┳━┓
        ┃S┃ ┃ ┃W┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃U┃ ┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━┫
        ┃F┃ ┃ ┃ ┃
        ┗━┻━┻━┻━┛
        """

        exp_name = 'experiment4'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        save_perf_path = os.path.join(save_dir_path, 'performance.txt')
        cls.make_directory(save_dir_path)

        # Functions
        water_reward_func = lambda x: 50.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 3),),
                'fire_locations': ((3, 0),),
                'obstacles': ((1, 2),),
            }

        # Run experiment
        num_episodes = 500
        reward_funcs = [water_reward_func, fire_reward_func]

        # Agents
        train_agent = Hopper(mode)
        verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                             ver_type=ver_type)

        if ver_type == 'line':
            verification_training = cls.peri_verification_training
            cache_results = False
        elif ver_type == 'forward-sim': 
            verification_training = cls.peri_verification_training
            cache_results = False
        elif ver_type == 'reach-tube':
            verification_training = cls.pre_verification_training
            cache_results = True
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Verify
        performance = verification_training(mode, train_agent, verify_agent, reset_func,
                                            reward_funcs, num_episodes, ver_type,
                                            save_agent_dir_path=save_dir_path,
                                            save_ver_path=save_ver_path,
                                            save_perf_path=save_perf_path,
                                            cache_results=cache_results)

    @classmethod
    def experiment4_5(cls, ver_type):
        """ Experiment4_5 environment
        ┏━┳━┳━┳━┓
        ┃S┃ ┃ ┃W┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃U┃ ┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━┫
        ┃F┃ ┃ ┃ ┃
        ┗━┻━┻━┻━┛
        """

        exp_name = 'experiment4_5'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        save_perf_path = os.path.join(save_dir_path, 'performance.txt')
        cls.make_directory(save_dir_path)

        # Functions
        water_reward_func = lambda x: 100.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 3),),
                'fire_locations': ((3, 0),),
                'obstacles': ((1, 2),),
            }

        # Fire propagation
        rate = 16
        influence = np.array(((1.0, 1.0, 0.0),))
        fire_prop = FirePropagation(influence=influence, rate=rate)

        # Run experiment
        num_episodes = 1000
        reward_funcs = [water_reward_func, fire_reward_func]

        # Agents
        train_agent = Hopper(mode)
        verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                             ver_type=ver_type)

        if ver_type == 'line':
            verification_training = cls.peri_verification_training
            cache_results = False
            actor_lr, critic_lr = 5e-5, 5e-4
        elif ver_type == 'forward-sim':
            verification_training = cls.peri_verification_training
            cache_results = False
            actor_lr, critic_lr = 5e-5, 5e-4
        elif ver_type == 'reach-tube':
            verification_training = cls.pre_verification_training
            cache_results = True
            actor_lr, critic_lr = 5e-5, 5e-4
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Verify
        verification_training(mode, train_agent, verify_agent, reset_func,
                              reward_funcs, num_episodes, ver_type,
                              save_agent_dir_path=save_dir_path,
                              save_ver_path=save_ver_path,
                              save_perf_path=save_perf_path,
                              cache_results=cache_results,
                              fire_propagation=fire_prop,
                              actor_lr=actor_lr, critic_lr=critic_lr)


    @classmethod
    def experiment5(cls, ver_type):
        """ Experiment5 environment
        ┏━┳━┳━┳━┓
        ┃S┃ ┃ ┃W┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃U┃ ┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃U┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃F┃U┃
        ┗━┻━┻━┻━┛
        """

        exp_name = 'experiment5'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        save_perf_path = os.path.join(save_dir_path, 'performance.txt')
        cls.make_directory(save_dir_path)

        # Functions
        water_reward_func = lambda x: 100.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 3),),
                'fire_locations': ((3, 2),),
                'obstacles': ((1, 2), (2, 3), (3, 3)),
            }

        rate = 16
        influence = np.array(((0.0, 1.0, 1.0),))
        fire_prop = FirePropagation(influence=influence, rate=rate)

        # Run experiment
        num_episodes = 1500
        reward_funcs = [water_reward_func, fire_reward_func]

        # Agents
        train_agent = Hopper(mode)
        verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                             ver_type=ver_type)

        if ver_type == 'line':
            verification_training = cls.peri_verification_training
            cache_results = False
            actor_lr, critic_lr = 5e-5, 5e-4
        elif ver_type == 'forward-sim':
            verification_training = cls.peri_verification_training
            cache_results = False
            actor_lr, critic_lr = 5e-5, 5e-4
        elif ver_type == 'reach-tube':
            verification_training = cls.pre_verification_training
            cache_results = True
            actor_lr, critic_lr = 5e-5, 5e-4
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Verify
        verification_training(mode, train_agent, verify_agent, reset_func,
                              reward_funcs, num_episodes, ver_type,
                              save_agent_dir_path=save_dir_path,
                              save_ver_path=save_ver_path,
                              save_perf_path=save_perf_path,
                              cache_results=cache_results,
                              fire_propagation=fire_prop,
                              actor_lr=actor_lr, critic_lr=critic_lr)

    @classmethod
    def experiment6(cls, ver_type):
        """ Experiment6 environment
        ┏━┳━┳━┳━┓
        ┃S┃ ┃ ┃W┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃U┃ ┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃U┃
        ┣━╋━╋━╋━┫
        ┃U┃ ┃F┃U┃
        ┗━┻━┻━┻━┛

        """

        exp_name = 'experiment6'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        save_perf_path = os.path.join(save_dir_path, 'performance.txt')
        cls.make_directory(save_dir_path)

        # Functions
        water_reward_func = lambda x: 50.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 3),),
                'fire_locations': ((3, 2),),
                'obstacles': ((1, 2), (2, 3), (3, 0), (3, 3)),
            }

        # Run experiment
        num_episodes = 750
        reward_funcs = [water_reward_func, fire_reward_func]

        # Agents
        train_agent = Hopper(mode)
        verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                             ver_type=ver_type)

        if ver_type == 'line':
            verification_training = cls.peri_verification_training
            cache_results = False
            actor_lr, critic_lr = 1e-4, 1e-3
        elif ver_type == 'forward-sim': 
            verification_training = cls.peri_verification_training
            cache_results = False
            actor_lr, critic_lr = 1e-4, 1e-3
        elif ver_type == 'reach-tube':
            verification_training = cls.pre_verification_training
            cache_results = True
            actor_lr, critic_lr = 5e-5, 5e-4
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Verify
        verification_training(mode, train_agent, verify_agent, reset_func,
                              reward_funcs, num_episodes, ver_type, 
                              save_agent_dir_path=save_dir_path,
                              save_ver_path=save_ver_path, 
                              save_perf_path=save_perf_path,
                              cache_results=cache_results,
                              actor_lr=actor_lr, critic_lr=critic_lr)


    @classmethod
    def experiment7(cls, ver_type):
        """ Experiment7 environment
        ┏━┳━┳━┳━┓
        ┃S┃ ┃ ┃W┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃F┃U┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃U┃
        ┣━╋━╋━╋━┫
        ┃ ┃ ┃ ┃U┃
        ┗━┻━┻━┻━┛
        """

        exp_name = 'experiment7'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        save_perf_path = os.path.join(save_dir_path, 'performance.txt')
        cls.make_directory(save_dir_path)

        # Functions
        water_reward_func = lambda x: 100.0
        def fire_reward_func(fire_extent):
            reward_arr = [60, 45, 30, 15]
            return reward_arr[fire_extent]

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 3),),
                'fire_locations': ((1, 2),),
                'obstacles': ((1, 3), (2, 3), (3, 3)),
            }

        rate = 5
        influence = np.array([[0.0, 1.0, 0.0],
                              [0.0, 0.0, 1.0],
                              [0.0, 0.0, 0.0]])
        fire_prop = FirePropagation(influence=influence, rate=rate)

        # Run experiment
        num_episodes = 2500
        reward_funcs = [water_reward_func, fire_reward_func]

        # Agents
        train_agent = Hopper(mode)
        verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                             ver_type=ver_type)

        if ver_type == 'line':
            verification_training = cls.peri_verification_training
            cache_results = False
            actor_lr, critic_lr = 1e-5, 1e-4
        elif ver_type == 'forward-sim':
            verification_training = cls.peri_verification_training
            cache_results = False
            actor_lr, critic_lr = 1e-5, 1e-4
        elif ver_type == 'reach-tube':
            verification_training = cls.pre_verification_training
            cache_results = True
            actor_lr, critic_lr = 1e-5, 1e-4
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Verify
        verification_training(mode, train_agent, verify_agent, reset_func,
                              reward_funcs, num_episodes, ver_type,
                              save_agent_dir_path=save_dir_path,
                              save_ver_path=save_ver_path,
                              save_perf_path=save_perf_path,
                              cache_results=cache_results,
                              fire_propagation=fire_prop,
                              actor_lr=actor_lr, critic_lr=critic_lr)



class Env5x5(Experiment):
    env_name = 'env5x5'
    env_shape = (5, 5)
    action_shape = (2,)
    input_shape = (28,)

    @classmethod
    def experiment1(cls, ver_type):
        """ Experiment1 environment
        ┏━┳━┳━┳━┳━┓
        ┃S┃ ┃ ┃ ┃W┃
        ┣━╋━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━╋━┫
        ┃ ┃ ┃U┃ ┃ ┃
        ┣━╋━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━╋━┫
        ┃F┃ ┃ ┃ ┃ ┃
        ┗━┻━┻━┻━┻━┛
        """

        exp_name = 'experiment1'
        mode = 'fire'

        # Create directory
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR,
                                                     cls.env_name,
                                                     exp_name, ver_type))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')
        cls.make_directory(save_dir_path)

        """
        # Fire
        rate = 12
        influence = np.array(((0.0, 0,0, 0,0),
                              (1.0, 1.0, 0.0),
                              (0,0, 1.0, 0.0)))
        fire_propagation = FirePropagation(influence=influence, rate=rate)
        """

        # Functions
        water_reward_func = lambda x: 50.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((0, 4),),
                'fire_locations': ((4, 0),),
                'obstacles': ((2, 2),),
            }

        # Run experiment
        num_episodes = 2500
        reward_funcs = [water_reward_func, fire_reward_func]

        # Agents
        train_agent = Hopper(mode)
        verify_agent = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode,
                             ver_type=ver_type)

        if ver_type == 'line':
            verification_training = cls.peri_verification_training
            cache_results = False
        elif ver_type == 'forward-sim': 
            verification_training = cls.peri_verification_training
            cache_results = False
        elif ver_type == 'reach-tube':
            verification_training = cls.pre_verification_training
            cache_results = True
        else:
            raise ValueError('Invalid ver_type: {}'.format(ver_type))

        # Verify
        '''
        performance = verification_training(mode, train_agent, verify_agent, reset_func,
                                            reward_funcs, num_episodes,
                                            save_agent_dir_path=save_dir_path,
                                            save_ver_path=save_ver_path,
                                            cache_results=cache_results)
        cls.print_performance(performance, save_perf_path)
        '''

        cls.run_policy(mode, train_agent, verify_agent, reset_func,
                       reward_funcs, save_dir_path)

class Env8x8(Experiment):
    env_name = 'env8x8'
    env_shape = (8, 8)
    action_shape = (2,)
    input_shape = (67,)

    sess = tf.Session()

    @classmethod
    def experiment1(cls):
        """ Experiment1 environment
        ┏━┳━┳━┳━┳━┳━┳━┳━┓
        ┃S┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━╋━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━╋━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃ ┃U┃U┃U┃
        ┣━╋━╋━╋━╋━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃ ┃
        ┣━╋━╋━╋━╋━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃ ┃ ┃U┃ ┃
        ┣━╋━╋━╋━╋━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃U┃ ┃U┃ ┃
        ┣━╋━╋━╋━╋━╋━╋━╋━┫
        ┃ ┃ ┃F┃ ┃ ┃ ┃U┃ ┃
        ┣━╋━╋━╋━╋━╋━╋━╋━┫
        ┃ ┃ ┃ ┃ ┃ ┃ ┃U┃W┃
        ┗━┻━┻━┻━┻━┻━┻━┻━┛
        """

        exp_name = 'experiment1'
        mode = 'fire'

        # Agents
        hopper = Hopper(mode)
        drone = Drone(20.0, 0.1, dims=len(cls.env_shape), mode=mode)

        # Fire
        rate = 16
        influence = np.array(((0.0, 0.0, 0.0),
                              (1.0, 1.0, 0.0),
                              (0.0, 1.0, 0.0)))
        fire_propagation = FirePropagation(influence=influence, rate=rate)

        # Functions
        water_reward_func = lambda x: 50.0
        fire_reward_func = lambda x: 25.0

        def reset_func():
            return {
                'start': (0, 0),
                'water_reservoirs': ((7, 7),),
                'fire_locations': ((7, 0),),
                'obstacles': ((2, 5), (2, 6), (2, 7), (5, 4), (6, 4), (7, 4),
                              (5, 6)),
            }

        # Run experiment
        num_episodes = 7500
        reward_funcs = [water_reward_func, fire_reward_func]
        save_dir_path = os.path.abspath(os.path.join(EXP_DIR, cls.env_name, exp_name))
        save_ver_path = os.path.join(save_dir_path, 'verification.dat')

        # Pre-verify
        cls.pre_verification_training(mode, hopper, drone, reset_func,
                                      reward_funcs, num_episodes,
                                      'reach-tube', save_agent_dir_path=save_dir_path,
                                      save_ver_path=save_ver_path,
                                      fire_propagation=fire_propagation,
                                      actor_lr=1e-6, critic_lr=1e-5)

