from __future__ import absolute_import, division, print_function

from agents.continuous.drone import Drone
from scipy.integrate import odeint
import math as m
import numpy as np

def TC_Simulate(mode, initial_condition, time_bound):
    action = [0.0, 0.0, 0.0, 0.0]
    input_state = [float(i_c) for i_c in initial_condition]

    drone = Drone(20.0, 0.1) 
    time_step = drone._time_step
    time_bound = float(time_bound)

    number_points = int(np.ceil(time_bound / time_step))
    time = [i * time_step for i in range(0, number_points)]
    if time[-1] != time_bound:
        time.append(time_bound)
    time_seq = np.arange(0.0, time_step, time_step / 10)
    drone.reset(input_state)

    # Simulate the system
    (goal_x, goal_y) = Drone.GOAL_POS
    trace = []
    for i, t in enumerate(time):
        desired_state = drone._compute_desired_state(Drone.GOAL_POS)
        action = drone._control(desired_state, action)

        state = drone.get_state()[:9]
        out = odeint(drone._dynamics, state, time_seq, args=(action,))
        drone.set_state(out[-1])

        # Construct trace
        trace.append([i] + list(drone.get_state()[:9]))

    return np.array(trace)
