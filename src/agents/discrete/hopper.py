from namedlist import namedlist
from lib.helper import flatten
#from sympy.geometry import Polygon, Segment

class Hopper:
    def __init__(self, mode):
        self.name = 'hopper'
        self._mode = mode

        State = namedlist('State', 'pos cur_goal water_level', default=None)
        self._state = State()
        self._water_capacity = 1

    @property
    def current_goal(self):
        return self._state.cur_goal

    @property
    def state(self):
        if self._mode == 'fire':
            return flatten((self._state.pos, self._state.water_level))

        elif self._mode == 'goal':
            return flatten((self._state.pos, self._state.cur_goal))

    @property
    def water_level(self):
        return self._state.water_level

    def fill_water(self, amount=-1):
        if amount < 0:
            self._state.water_level = self._water_capacity
        else:
            self._state.water_level = min(self._water_capacity,
                                          self._state.water_level + amount)

    def release_water(self, amount=-1):
        if amount < 0:
            released = self._state.water_level
            self._state.water_level = 0
            return released

        released = min(self._state.water_level, amount)
        self._state.water_level -= released
        return released

    def reset(self, pos, cur_goal=None, water_level=None):
        self._state.pos = pos
        
        if self._mode == 'fire':
            self._state.water_level = water_level

        elif self._mode == 'goal':
            self._state.cur_goal = cur_goal
    
    def get_position(self):
        return self._state.pos

    def set_position(self, pos):
        self._state.pos = pos

    def move_position(self, pos):
        self._state.pos = pos

    def increment_cur_goal(self):
        self._state.cur_goal += 1

    '''
    def verify(self, goal, env_shape, obstacles=None):
        # Create unsafe sets
        unsafe_sets = []
        if not obstacles is None:
            for obs in obstacles:
                unsafe_sets.append(self._create_Rectangle(obs, env_shape))

        # Create a segment
        if self._state.pos == goal:
            return 'SAFE', None
        trajectory = self._create_Segment(self._state.pos, goal, env_shape)

        # Check each segment for unsafety
        for unsafe_set in unsafe_sets:
            point = unsafe_set.intersect(trajectory)
            if not point.is_EmptySet:
                return 'UNSAFE', point

        return 'SAFE', None

    def _create_Rectangle(self, center, env_shape):
        center = self._transform_matrix_to_coord(center, env_shape)
        return Polygon((center[0] - 0.5, center[1] + 0.5),
                       (center[0] + 0.5, center[1] + 0.5),
                       (center[0] + 0.5, center[1] - 0.5),
                       (center[0] - 0.5, center[1] - 0.5))

    def _create_Segment(self, pos, goal, env_shape):
        p1 = self._transform_matrix_to_coord(pos, env_shape)
        p2 = self._transform_matrix_to_coord(goal, env_shape)
        return Segment(p1, p2)

    @staticmethod
    def _transform_matrix_to_coord(m, env_shape):
        if len(env_shape) == 1:
            return m
        elif len(env_shape) == 2:
            return (m[1], env_shape[0] - 1 - m[0])
        else:
            raise ValueError('Not implemented for 3D')
    '''
