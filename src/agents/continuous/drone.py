from __future__ import absolute_import, division, print_function
from os import path
from sympy.geometry import Polygon, Segment
import sys

sys.path.append(path.abspath("../../"))     # For cloning repo inside DryVR
sys.path.append(path.abspath("../../../"))  # For past setup during development

import math as m
import numpy as np
from namedlist import namedlist
from lib.helper import flatten
from scipy.integrate import odeint
from src.common.utils import importSimFunction
from src.core.dryvrmain import verify

import pdb

class Drone:
    G = 9.81
    GOAL_POS = (-1, -1)

    def __init__(self, mass, time_step, dims=2, mode='fire', ver_type='reach-tube'):
        self.name = 'drone'

        self._dims = dims
        self._mode = mode
        self._mass = mass
        self._ver_type = ver_type
        self._time_step = time_step
        self._time_seq = np.arange(0.0, self._time_step, self._time_step / 10.0)

        self._State = namedlist('State',
                                [('x', 0.0), ('y', 0.0), ('z', 0.0),
                                 ('vx', 0.0), ('vy', 0.0), ('vz', 0.0),
                                 ('phi', 0.0), ('theta', 0.0), ('psi', 0.0),
                                 ('cur_goal', 0), ('water_level', 0.0)])
        self._action = np.zeros(4)
        self._state = self._State()

        self._water_capacity = 1

    @property
    def current_goal(self):
        return self._state.cur_goal

    @property
    def water_level(self):
        return self._state.water_level

    @property
    def state(self):
        pos = self._convert_state_to_pos()
        if self._mode == 'fire':
            return flatten((pos, self._state.water_level))
        elif self._mode == 'goal':
            return flatten((pos, self._state.cur_goal))

    def reset(self, state, cur_goal=1, water_level=0.0):
        self._actions = np.zeros(4)
        self._state = self._State()

        # Reverse if state is index
        if len(state) <= 3:
            state = state[::-1]

        for i, v in enumerate(state):
            self._state[i] = float(v)

        if self._mode == 'fire':
            self._state.water_level = water_level

        elif self._mode == 'goal':
            self._state.cur_goal = cur_goal

    def fill_water(self, amount=-1):
        if amount < 0:
            self._state.water_level = self._water_capacity
        else:
            self._state.water_level = min(self._water_capacity,
                                          self._state.water_level + amount)

    def release_water(self, amount=-1):
        if amount < 0:
            released = self._state.water_level
            self._state.water_level = 0
            return released

        released = min(self._state.water_level, amount)
        self._state.water_level -= released
        return released

    def get_position(self):
        return self._convert_state_to_pos()

    def set_position(self, pos):
        new_pos = pos[::-1]
        for i, v in enumerate(new_pos):
            self._state[i] = float(v)

    def get_state(self):
        return self._state[:]

    def set_state(self, state):
        for i, v in enumerate(state):
            self._state[i] = float(v)

    def quantize_state(self, pos):
        self.set_position(pos)
        for i in range(3, 8):
            self._state[i] = 0.0

    def increment_cur_goal(self):
        self._state.cur_goal += 1

    def move_position(self, pos, render=None):
        assert len(pos) == 2

        goal = pos[:]
        pos = pos[::-1]
        while True:
            state = self._update(pos)

            if not render is None:
                render(goal)

            (x, y), (vx, vy) = state[:2], state[3:5]
            if (m.sqrt((x - pos[0]) ** 2 + (y - pos[1]) ** 2) < 0.125 and
                    abs(vx) < 0.1 and abs(vy) < 0.1):
                break

    def verify(self, goal, env_shape, obstacles, mode):
        if mode == 'forward-sim':
            result, _ = self._verify_forward_sim(goal, env_shape, obstacles)
        elif mode == 'reach-tube':
            result, _ = self._verify_reach_tube(goal, env_shape, obstacles)
        else:
            result, _ = self._verify_line(goal, env_shape, obstacles)

        safe = True if result == 'SAFE' else False
        return safe

    def _verify_line(self, goal, env_shape, obstacles):
        # Create unsafe sets
        unsafe_sets = []
        for obs in obstacles:
            unsafe_sets.append(self._create_Rectangle(obs, env_shape))

        # Create a segment
        if goal == self._convert_state_to_pos():
            return 'SAFE', None
        pos = self.get_position()
        trajectory = self._create_Segment(pos, goal, env_shape)

        # Check each segment for unsafety
        for unsafe_set in unsafe_sets:
            point = unsafe_set.intersect(trajectory)
            if not point.is_EmptySet:
                return 'UNSAFE', point

        return 'SAFE', None

    def _create_Rectangle(self, center, env_shape):
        center = self._transform_matrix_to_coord(center, env_shape)
        return Polygon((center[0] - 0.5, center[1] + 0.5),
                       (center[0] + 0.5, center[1] + 0.5),
                       (center[0] + 0.5, center[1] - 0.5),
                       (center[0] - 0.5, center[1] - 0.5))

    def _create_Segment(self, pos, goal, env_shape):
        p1 = self._transform_matrix_to_coord(pos, env_shape)
        p2 = self._transform_matrix_to_coord(goal, env_shape)
        return Segment(p1, p2)

    @staticmethod
    def _transform_matrix_to_coord(m, env_shape):
        if len(env_shape) == 1:
            return m
        elif len(env_shape) == 2:
            return (m[1], env_shape[0] - 1 - m[0])
        else:
            raise ValueError('Not implemented for 3D')

    def _verify_forward_sim(self, goal, env_shape, obstacles):
        # Set verification parameter
        Drone.GOAL_POS = goal[::-1]

        # Set up JSON
        set_lower, set_higher = self._state[:9], self._state[:9]
        initial_set = [list(set_lower), list(set_higher)]

        # Add obstacles to unsafe set
        obs_set_str = ''
        for obs in obstacles:
            obs_pos = obs[::-1]
            obs_x_str = 'x>={}, x<={}'.format(obs_pos[-2] - 0.5, obs_pos[-2] + 0.5)
            obs_y_str = 'y>={}, y<={}'.format(obs_pos[-1] - 0.5, obs_pos[-1] + 0.5)
            obs_set_str += 'And({}, {}), '.format(obs_x_str, obs_y_str)

        # Create unsafe set
        unsafe_set = '@Allmode:Or(' + obs_set_str + ')'

        # Set up JSON data 
        #time_horizon = 15.0
        time_horizon = 10.0
        json_data = {
            "vertex": ["Default"],
            "edge": [],
            "variables": ["x", "y", "z", "vx", "vy", "vz", "phi", "theta", "psi"],
            "guards": [],
            "initialVertex": 0,
            "initialSet": initial_set,
            "unsafeSet": unsafe_set,
            "timeHorizon": time_horizon,
            "directory": "boeing/fire-drone/src",
        }

        # Verify
        param_config = {'REFINETHRES': 0}
        sim_function = importSimFunction(json_data["directory"])
        safety, reach = verify(json_data, sim_function,
                               paramConfig=param_config)
        
        # Fix safety for 'forward-sim'
        if safety == 'UNKNOWN':
            safety = 'SAFE'

        return safety, reach

    def _verify_reach_tube(self, goal, env_shape, obstacles):
        # Set verification parameter
        Drone.GOAL_POS = goal[::-1]

        # Create initial states
        init_cell_half_width = 0.125
        lower = np.round(self._state[:2]) - init_cell_half_width
        higher = lower + init_cell_half_width * 2.0

        set_lower = list(lower) + [4.95, -0.1, -0.1, -0.01, -0.01, -0.01, 0.0]
        set_higher = list(higher) + [5.05, 0.1, 0.1, 0.01, 0.01, 0.01, 2 * np.pi - 1e-6]
        initial_set = [list(set_lower), list(set_higher)]

        """
        set_lower, set_higher = np.zeros(9), np.zeros(9)
        set_lower[:2], set_lower[2], set_lower[8] = lower, 4.95, 0.0
        set_higher[:2], set_higher[2], set_higher[8] = higher, 5.05, 2 * np.pi - 1e-6

        # Velocities
        set_lower[3], set_lower[4] = -0.1, -0.1
        set_higher[3], set_higher[4] = 0.1, 0.1
        """


        # Add obstacles to unsafe set
        obs_set_str = ''
        for obs in obstacles:
            obs_pos = obs[::-1]
            obs_x_str = 'x>={}, x<={}'.format(obs_pos[-2] - 0.5, obs_pos[-2] + 0.5)
            obs_y_str = 'y>={}, y<={}'.format(obs_pos[-1] - 0.5, obs_pos[-1] + 0.5)
            obs_set_str += 'And({}, {}), '.format(obs_x_str, obs_y_str)

        # Create unsafe set
        unsafe_set = '@Allmode:Or(' + obs_set_str + ')'

        '''
        # Set up JSON data
        pi_8 = str(np.pi / 8)

        initial_set_lower = [lower[0], lower[1], 4.95, -0.1, -0.1, -0.001, -0.01, -0.01, 0.0]
        initial_set_higher = [higher[0], higher[1], 5.05, 0.1, 0.1, 0.001, 0.01, 0.01, 0.0]
        initial_set = [initial_set_lower, initial_set_higher]

        unsafe_set = ("@Allmode:Or("
                      "x<=-1.0, x>=9.0, "
                      "y<=-1.0, y>=9.0, "
                      "z<=4.9, z>=5.1, "
                      "vx<=-1.0, vx>=1.0, "
                      "vy<=-1.0, vy>=1.0, "
                      "vz<=-0.1, vz>=0.1, "
                      "phi<=-" + pi_8 + ", phi>=" + pi_8 + ", "
                      "theta<=-" + pi_8 + ", theta>=" + pi_8 + ", "
                      ")"
                     )
        '''

        time_horizon = 15.0
        json_data = {
            "vertex": ["Default"],
            "edge": [],
            "variables": ["x", "y", "z", "vx", "vy", "vz", "phi", "theta", "psi"],
            "guards": [],
            "initialVertex": 0,
            "initialSet": initial_set,
            "unsafeSet": unsafe_set,
            "timeHorizon": time_horizon,
            "directory": "boeing/fire-drone/src",

            'bloatingMethod': 'PW_POTOK',
            #"bloatingMethod": 'PW',
            #'kvalue': [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
        }

        # Verify
        param_config = {'REFINETHRES': 10, 'SIMTRACENUM': 10}
        sim_function = importSimFunction(json_data["directory"])
        safety, reach = verify(json_data, sim_function,
                               paramConfig=param_config)

        return safety, reach

    def _control(self, desired, action):
        # Constants
        Kp, Kp_bar, Kd = 0.9, 0.1, 0.1

        # Variables
        (x, y, z, vx, vy, vz, phi, theta, psi) = self._state[:9]
        (x_d, y_d, z_d, vx_d, vy_d, vz_d, phi_d, theta_d, psi_d,
         ax_d, ay_d, az_d, dphi_d, dtheta_d, dpsi_d) = desired

        # Derivative of state
        (_, _, _, _, _, _, dphi, dtheta, dpsi) = self._dynamics(self._state[:9], 1.0, action)

        # Feedforward control
        f_ff = -self._mass * m.sqrt(ax_d ** 2 + ay_d ** 2 + (az_d - self.G) ** 2)
        w1_ff = dphi_d - m.sin(theta_d) * dpsi_d
        w2_ff = m.cos(phi_d) * dtheta_d + m.sin(phi_d) * m.cos(theta_d) * dpsi_d
        w3_ff = -m.sin(phi_d) * dtheta_d + m.cos(phi_d) * m.cos(theta_d) * dpsi_d

        # Feedback control
        f_fbx = ((m.cos(phi) * m.sin(theta) * m.cos(psi) + m.sin(phi) * m.sin(psi)) *
                 ((x_d - x) * Kp + (vx_d - vx) * Kd))
        f_fby = ((m.cos(phi) * m.sin(theta) * m.sin(psi) - m.sin(phi) * m.cos(psi)) *
                 ((y_d - y) * Kp + (vy_d - vy) * Kd))
        f_fbz = m.cos(phi) * m.cos(theta) * ((z_d - z) * Kp + (vz_d - vz) * Kd)
        f_fb = f_fbx + f_fby + f_fbz

        w1_fb = Kp * (phi_d - phi) + Kd * (dphi_d - dphi) + Kp_bar * (y_d - y)
        w2_fb = Kp * (theta_d - theta) + Kd * (dtheta_d - dtheta) + Kp_bar * (x_d - x)
        w3_fb = Kp * (psi_d - psi) + Kd * (dpsi_d - dpsi)

        return [f_ff + f_fb, w1_ff + w1_fb, w2_ff + w2_fb, w3_ff + w3_fb]

    def _compute_desired_state(self, goal):
        # State variables
        (x, y, z, vx, vy, vz, phi, theta, psi) = self._state[:9]
        
        # Compute distance and angle to goal
        v2 = np.array([goal[0] - x, goal[1] - y])
        dist = np.linalg.norm(v2)
        angle = self._compute_angle(psi, v2)

        # Compute desired state
        if np.linalg.norm(v2) >= 1:
            con = v2 / dist
        else:
            con = v2

        x_d = con[0] * self._time_step + x
        y_d = con[1] * self._time_step + y
        z_d = (5.0 - z) * self._time_step + z

        vx_d = 0.5 * con[0]
        vy_d = 0.5 * con[1]
        vz_d = 5.0 - z

        ax_d = (vx_d - vx)
        ay_d = (vy_d - vy)
        az_d = vz_d - vz

        psi_d = angle
        beta_a = -ax_d * m.cos(psi_d) - ay_d * m.sin(psi_d)
        beta_b = -az_d + self.G
        beta_c = -ax_d * m.sin(psi_d) + ay_d * m.cos(psi_d)
        theta_d = m.atan2(beta_a, beta_b)
        phi_d = m.atan2(beta_c, m.sqrt(beta_a ** 2 + beta_b ** 2))

        dphi_d = -phi
        dtheta_d = -theta
        dpsi_d = psi_d - psi
        
        desired = [x_d, y_d, z_d, vx_d, vy_d, vz_d, phi_d, theta_d, psi_d, 
                   ax_d, ay_d, az_d, dphi_d, dtheta_d, dpsi_d]
        return desired

    def _convert_state_to_pos(self):
        return tuple([int(round(p)) for p in self._state[:self._dims][::-1]])

    def _dynamics(self, state, time, action):
        # Variables
        (_, _, _, vx, vy, vz, phi, theta, psi) = state[:9]
        (fz, w1, w2, w3) = action[:]

        # Derivatives
        dvx = (m.cos(phi) * m.sin(theta) * m.cos(psi) + m.sin(phi) * m.sin(psi)) * fz / self._mass
        dvy = (m.cos(phi) * m.sin(theta) * m.sin(psi) - m.sin(phi) * m.cos(psi)) * fz / self._mass
        dvz = m.cos(phi) * m.cos(theta) * fz / self._mass + self.G

        dphi = w1 + m.sin(phi) * m.tan(theta) + w2 + m.cos(phi) * m.tan(theta) * w3
        dtheta = m.cos(phi) * w2 - m.sin(phi) * w3
        dpsi = m.sin(phi) * (1 / m.cos(theta)) * w2 + m.cos(phi) * (1 / m.cos(theta)) * w3

        return [vx, vy, vz, dvx, dvy, dvz, dphi, dtheta, dpsi]

    def _update(self, goal):
        desired_state = self._compute_desired_state(goal)
        self._action = self._control(desired_state, self._action)

        solution = odeint(self._dynamics, self._state[:9], self._time_seq,
                          args=(self._action,))

        for i, v in enumerate(solution[-1]):
            self._state[i] = v

        return np.copy(self._state)

    @staticmethod
    def _compute_angle(psi, vec):
        unit = np.array([1.0, 0.0])
        c, s = m.cos(psi), m.sin(psi)
        R = np.array(((c, -s), (s, c)))

        heading = np.matmul(R, unit)
        diff = np.arctan2(np.linalg.det([heading, vec]), np.dot(heading, vec)) 
        return psi + diff
