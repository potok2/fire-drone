# fire-drone

## Description

fire-drone is a project to develop a certifiable AI system for an autonomous fire-fighting
drone. 

## Project Organization

The project is organized into 4 main submodules. Each submodule contains
directories for continuous and discrete components. Some of the submodules may
be missing one of these components if no files were created for that components.

The linkages between the various submodules is illustrated below:

![Software Architecture](figures/software-architecture.png)

All of the modules on the lower level are combined together within the
experiment submodule.

* `agents/` - collection of agents that interact with the environment.
    * `discrete/hopper.py` - a discrete agent that 'hops' between locations in
      the enivornment.
    * `continuous/drone.py` - a continuous agents whose dynamics are defined by
      [#wang2017safe]. 
* `environments/` - collection of environments and related models
    * `discrete/hyper_plane.py` - a model for an n-dimensional grid-like
      environment.
    * `discrete/fire_propagation.py` - a model for convolution based fire
      propagation.
* `experiments/` - definitions for running experiments.
    * `discrete/experiments.py` - defines the three main algorithms for running.
      experiments: pre-, peri-, and post- within a class structure for ease of
      testing againts various agent/environment configurations.
* `lib/` - directory of additional helper functions.
    * `helper.py` - a collection of useful auxilary functions.
    * `plotting.py` - functions for plotting data output by the policy.
* `policies/` - container for various policy implementations.
    * `discrete/actor_critic.py` - an implementation of Actor-Critic
      architecture that can output 1D, 2D, or 3D coordinates.
* `dryvry_verify.py` - contains the `TC_Simulate` that will be called by DryVR
  to simulate the dynamics of the drone
* `main.py` - main script to run the project

## Installation/Setup

To be able to run the project, first install DryVR following the instructions
located [here](https://dryvr-02.readthedocs.io/en/latest/) in a location of your
choosing. Next, clone this directory inside the DryVR directory. Finally, use
`pip` to install all necessary requirements:
```
pip install -r requirements.txt
```

## Usage

To run the the project, navigate into the `src/` directory and execute
```
python main.py
```

This will run the experiments defined inside `main.py`. To run other
experiments, simply modify the contents of this file.

## Additional Notes

This section contains a list of importants notes about the project.

* The `dryvr_verify.py` file has to be in the same directory path as `main.py`
  in order for the static variable `Drone.GOAL_POS` to contain the correct
  value. If the file is not in the same level, DryVR will consider the
  `Drone` class inside the `TC_Simulate` function to be different from the
  `Drone` class used by main so the value of `Drone.GOAL_POS` will be incorrect.

* The agent `hopper.py` was subsumed by `drone.py` and may not be complete.
  Initially, `hopper.py` was used to drive the exploration process around the
  environment for faster processing but it became necessary to later verify the
  path of the agent as it actually moved around in the environment so this class
  ceased to be used. In some modules, you may see `train_agent` and
  `verify_agent` variables because of this reason.

[#wang2017safe]( L.   Wang,   E.   A.   Theodorou,   and   M.   Egerstedt,   “Safe   learning of   quadrotor   dynamics   using   barrier   certificates,” arXiv   preprint arXiv:1710.05472 , 2017)
