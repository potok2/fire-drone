# -*- coding: utf-8 -*-
"""
    @author: Hussein Sibai
    
"""

#own-aircraft state (position, velocity, water storage, gas storage...), states of the other aircrafts, position of the buildings, position of the fire, position of the pool, position of the landing position

import math as m
from mpmath import *
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
from scipy.integrate import odeint



class ContinuousFireDroneEnv(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self):
        self.min_action_1 = -1.0
        self.max_action_1 = 1.0
        self.min_action_2 = -1.0
        self.max_action_2 = 1.0
        self.min_action_3 = -1.0
        self.max_action_3 = 1.0
        self.min_action_4 = -1.0
        self.max_action_4 = 1.0
        self.min_action_water = -1.0
        self.max_action_water = 1.0
        
        self.min_x_position = -20
        self.max_x_position = 20
        self.min_y_position = -20
        self.max_y_position = 20
        self.min_z_position = 0
        self.max_z_position = 20
        self.min_x_velocity = -20
        self.max_x_velocity = 20
        self.min_y_velocity = -20
        self.max_y_velocity = 20
        self.min_z_velocity = -20
        self.max_z_velocity = 20
        self.min_x_acc = -20
        self.max_x_acc = 20
        self.min_y_acc = -20
        self.max_y_acc = 20
        self.min_z_acc = -20
        self.max_z_acc = 20
        
        self.min_psi= 0 # yaw
        self.max_psi = 2 * m.pi
        self.min_psi_velocity = 0 # yaw
        self.max_psi_velocity = 2 * m.pi
        self.min_psi_acc = 0 # yaw
        self.max_psi_acc = 2 * m.pi
        
        self.min_water_storage = 0
        self.max_water_storage = 10
        self.min_gas_storage = 0
        self.max_gas_storage = 10
        
        self.min_d_x = -5 # disturbance mean on different parameters
        self.max_d_x = 5
        self.min_d_y = -5
        self.max_d_y = 5
        self.min_d_z = -5
        self.max_d_z = 5
        self.min_d_vx = -5
        self.max_d_vx = 5
        self.min_d_vy = -5
        self.max_d_vy = 5
        self.min_d_vz = -5
        self.max_d_vz = 5
        self.min_d_phi = -5
        self.max_d_phi = 5
        self.min_d_theta = -5
        self.max_d_theta = 5
        self.min_d_psi = -5
        self.max_d_psi = 5
        
        self.x_1_fire = -20
        self.y_1_fire = 20
        self.x_2_fire = -20
        self.y_2_fire = 20
        self.fire_resolution = 5
        self.min_fire_extent = 0
        self.max_fire_extent = 10
        
        self.T = 1 # size of the time step before the state is sampled again and new action is given.
        
        self.num_x_cells = int(abs(self.x_2_fire - self.x_1_fire)/self.fire_resolution) # will include it in the reward function
        self.num_y_cells = int(abs(self.y_2_fire - self.y_1_fire)/self.fire_resolution)
        self.fire_extent = np.zeros((self.num_x_cells, self.num_y_cells))
        
        self.low_state = np.array([self.min_x_position, self.min_y_position, self.min_z_position, self.min_psi, self.min_x_velocity, self.min_y_velocity, self.min_z_velocity, self.min_psi_velocity, self.min_x_acc, self.min_y_acc, self.min_z_acc, self.min_psi_acc, self.min_water_storage, self.min_gas_storage])

        self.high_state = np.array([self.max_x_position, self.max_y_position, self.max_z_position, self.max_psi, self.max_x_velocity, self.max_y_velocity, self.max_z_velocity, self.max_psi_velocity, self.max_x_acc, self.max_y_acc, self.max_z_acc, self.max_psi_acc, self.max_water_storage, self.max_gas_storage])
        
        self.min_action = np.array([self.min_action_1, self.min_action_2, self.min_action_3, self.min_action_4, self.min_action_water])
        self.max_action = np.array([self.max_action_1, self.max_action_2, self.max_action_3, self.max_action_4, self.max_action_water])
        
        self.viewer = None
        
        self.action_space = spaces.Box(low=self.min_action, high=self.max_action)
        self.observation_space = spaces.Box(low=self.low_state, high=self.high_state)
        
        self.seed()
        self.reset()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]
    
    def quantize(self,r):
        xq = int(floor((r[0]-self.x_1_fire)/self.fire_resolution))
        yq = int(floor((r[1]-self.y_1_fire)/self.fire_resolution))
        return xq, yq
    
    def BaselineControl(s):
        x = s[0]
        y = s[1]
        z = s[2]
        vx = s[3]
        vy = s[4]
        vz = s[5]
        phi = s[6]
        theta = s[7]
        psi = s[8]
        water = s[9]
        
        g = 9.81
        mass = 200
        
        beta_a = -1*ax_d*m.cos(psi_d) - ay_d*m.sin(psi_d)
        beta_b = -1*az_d + g
        beta_c = -1*ax_d*m.sin(psi_d) + ay_d*m.cos(psi_d)
        
        theta_d = m.atan2(beta_a, beta_b)
        phi_d = m.atan2(beta_c, sqrt(beta_a*beta_a + beta_b * beta_b))
    
        f_ff = - mass * sqrt(ax_d * ax_d + ay_d * ay_d + (az_d - g)*(az_d - g))
        w1_ff = phid_d - m.sin(theta_d)*psid_d
        w2_ff = m.cos(phi_d)*thetad_d + m.sin(phi_d)*m.cos(theta_d)*psid_d
        w3_ff = -m.sin(phi_d)*thetad_d + m.cos(phi_d)*m.cos(theta_d)*psid_d
    
        f_fb = Kp * (-1*m.sin(theta)*(x_d - x) + m.sin(phi)*m.cos(theta)*(y_d - y) + m.cos(phi)*m.cos(theta)*(z_d - z)) + Kd *  (-1*m.sin(theta)*(vx_d - vx) + m.sin(phi)*m.cos(theta)*(vy_d - vy) + m.cos(phi)*m.cos(theta)*(vz_d - vz))
        w1_fb = Kp * (phi_d - phi) + Kd * (phid_d - phid) + (1 - Kp) * (y_d - y)
        w2_fb = Kp * (theta_d - theta) + Kd * (thetad_d - thetad) + (1 - Kp) * (x_d - x)
        w3_fb = Kp * (psi_d - psi) + Kd * (psid_d - psid)
    
        return [f_ff + f_fb, w1_ff + w1_fb, w2_ff + w2_fb, w3_ff + w3_fb]
    
    
    def DroneDynamics(s):
    
        x = s[0]
        y = s[1]
        z = s[2]
        vx = s[3]
        vy = s[4]
        vz = s[5]
        phi = s[6]
        theta = s[7]
        psi = s[8]
        water = s[9]
        gas = s[10]
    
    
        fz = self.action[0]
        w1 = self.action[1]
        w2 = self.action[2]
        w3 = self.action[3]
        drop = self.action[4]
    
        g = 9.81
        mass = 200
        #zw = np.array([[0],[0],[1]])
        #R = np.array([[m.cos(theta)*m.cos(psi), m.sin(phi)*m.sin(theta)*m.cos(psi) - m.cos(phi)*m.sin(psi), m.cos(phi)*m.sin(theta)*m.cos(psi) + m.sin(phi)*m.sin(psi)],
        #     [m.cos(theta)*m.cos(psi), m.sin(phi)*m.sin(theta)*m.cos(psi) + m.cos(phi)*m.sin(psi), m.cos(phi)*m.sin(theta)*m.cos(psi) - m.sin(phi)*m.sin(psi)],
        #     [-1*m.sin(theta), m.sin(phi)*m.cos(theta), m.cos(phi)*m.cos(theta)]])
        #rdd = np.matmul(np.array([g]),zw) + (1/m)*np.matmul(np.matmul(R,zw),np.array([fz]))
    
        xd = vx
        yd = vy
        zd = vz
    
        vxd = (m.cos(phi)*m.sin(theta)*m.cos(psi) + m.sin(phi)*m.sin(psi))*fz/mass
        vyd = (m.cos(phi)*m.sin(theta)*m.cos(psi) - m.sin(phi)*m.sin(psi))*fz/mass
        vzd = m.cos(phi)*m.cos(theta)*fz/mass + g
    
        phid = w1 + m.sin(phi) * m.tan(theta) * w2 + m.cos(phi) * m.tan(theta) * w3
        thetad = m.cos(phi) * w2 - m.sin(phi) * w3
        psid = m.sin(phi) * sec(theta) * w2 + m.cos(phi) * sec(theta) * w3
    
        gasd = -1*f # proportional to thrust
        waterd = -1 * drop # if drop water then the water reserve will decrease linearly
    
    
        # return the state derivatives
        return [xd, yd, zd, vxd, vyd, vzd, phid, thetad, psid, waterd, gasd]
    
    

    def step(self, a): # takes an action as input
    
        simstep = self.T/10.0
        Tseq = np.arange(0, self.T,simstep)
        
        #self.action = a
        self.action =
        
        new_state = odeint(DroneDynamics, [self.state, self.action], Tseq)
        self.state[0:11] = new_state[-1,:]
        x = self.state[0]
        y = self.state[1]
        z = self.state[2]
        vx = self.state[3]
        vy = self.state[4]
        vz = self.state[5]
        phi = self.state[6]
        theta = self.state[7]
        psi = self.state[8]
        water = self.state[9]
        gas = self.state[10]
        
        
        drop = self.action[4]
        
        if (vx > self.max_x_velocity): vx = self.max_x_velocity
        if (vx < self.min_x_velocity): vx = self.min_x_velocity
        
        if (vy > self.max_y_velocity): vx = self.max_y_velocity
        if (vy < self.min_y_velocity): vx = self.min_y_velocity
        
        if (vz > self.max_z_velocity): vx = self.max_z_velocity
        if (vz < self.min_z_velocity): vx = self.min_z_velocity
        
        if (x > self.max_x_position): x = self.max_x_position
        if (x < self.min_x_position): x = self.min_x_position
        
        if (y > self.max_y_position): y = self.max_y_position
        if (y < self.min_y_position): y = self.min_y_position
        
        if (z > self.max_z_position): z = self.max_z_position
        if (z < self.min_z_position): z = self.min_z_position
        
        if (x==self.min_x_position and vx<0): vx = 0
        if (y==self.min_y_position and vy<0): vy = 0
        if (z==self.min_z_position and vz<0): vz = 0
        
        if (x==self.max_x_position and vx>0): vx = 0
        if (y==self.max_y_position and vy>0): vy = 0
        if (z==self.max_z_position and vz>0): vz = 0
        
        
        reward = 0
        # I'm ignoring the gas and water loss for now.
        for i in range(0, new_state.shape[0]):
            rq = quantize(new_state[i,0:2])
            new_fire_extent = min(0, self.fire_extent[rq[0],rq[1]] - drop) # the decrease in fire extent
            reward = reward + new_fire_extent - self.fire_extent[rq[0],rq[1]]
            new_fire_extent =  self.fire_extent[rq[0],rq[1]]
        
        done = 1
        for i in range(0,self.num_x_cells):
            for j in range(0,self.num_y_cells):
                if self.fire_extent[i][j] > 0:
                    done = 0;
                    break;
        if done:
            reward = 100.0
        
        #reward-= math.pow(action[0],2)*0.1
        return self.state, reward, done, {}

    def reset(self):
        self.state = np.array([self.np_random.uniform(low=self.min_x_position, high=self.max_x_position),
                               self.np_random.uniform(low=self.min_y_position, high=self.max_y_position),
                               self.np_random.uniform(low=self.min_z_position, high=self.max_z_position),
                               self.np_random.uniform(low=self.min_x_velocity, high=self.max_x_velocity),
                               self.np_random.uniform(low=self.min_y_velocity, high=self.max_y_velocity),
                               self.np_random.uniform(low=self.min_z_velocity, high=self.max_z_velocity),
                               self.np_random.uniform(low=self.min_phi, high=self.max_phi),
                               self.np_random.uniform(low=self.min_theta, high=self.max_theta),
                               self.np_random.uniform(low=self.min_psi, high=self.max_psi),
                               self.np_random.uniform(low=self.min_water_storage, high=self.max_water_storage),
                               self.np_random.uniform(low=self.min_gas_storage, high=self.max_gas_storage)
                               ])
        return np.array(self.state)
    
    '''
    def render(self, mode='human'):
        screen_width = 600
        screen_height = 400
        
        world_width = self.max_position - self.min_position
        scale = screen_width/world_width
        dronewidth=40
        droneheight=20
        
        
        if self.viewer is None:
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(screen_width, screen_height)
            xs = np.linspace(self.min_x_position, self.max_x_position, 100)
            ys =  np.linspace(self.min_y_position, self.max_y_position, 100)
            xys = list(zip((xs-self.min_x_position)*scale, zip((ys-self.min_y_position)*scale))
            
            self.track = rendering.make_polyline(xys)
            self.track.set_linewidth(4)
            self.viewer.add_geom(self.track)
            
            clearance = 10
            
            l,r,t,b = -dronewidth/2, dronewidth/2, droneheight, 0
            drone = rendering.FilledPolygon([(l,b), (l,t), (r,t), (r,b)])
            drone.add_attr(rendering.Transform(translation=(0, clearance)))
            self.dronetrans = rendering.Transform()
            drone.add_attr(self.dronetrans)
            self.viewer.add_geom(drone)
            frontwheel = rendering.make_circle(droneheight/2.5)
            frontwheel.set_color(.5, .5, .5)
            frontwheel.add_attr(rendering.Transform(translation=(dronewidth/4,clearance)))
            frontwheel.add_attr(self.dronetrans)
            self.viewer.add_geom(frontwheel)
            backwheel = rendering.make_circle(droneheight/2.5)
            backwheel.add_attr(rendering.Transform(translation=(-dronewidth/4,clearance)))
            backwheel.add_attr(self.dronetrans)
            backwheel.set_color(.5, .5, .5)
            self.viewer.add_geom(backwheel)
            flagx = (self.goal_position-self.min_position)*scale
            flagy1 = self._height(self.goal_position)*scale
            flagy2 = flagy1 + 50
            flagpole = rendering.Line((flagx, flagy1), (flagx, flagy2))
            self.viewer.add_geom(flagpole)
            flag = rendering.FilledPolygon([(flagx, flagy2), (flagx, flagy2-10), (flagx+25, flagy2-5)])
            flag.set_color(.8,.8,0)
            self.viewer.add_geom(flag)
        
        pos = self.state[0]
        self.dronetrans.set_translation((pos-self.min_position)*scale, self._height(pos)*scale)
        self.dronetrans.set_rotation(math.cos(3 * pos))
        
    return self.viewer.render(return_rgb_array = mode=='rgb_array')
    
def close(self):
    if self.viewer: self.viewer.close()
'''