# -*- coding: utf-8 -*-
"""
    @author: Hussein Sibai
    
"""

#own-aircraft state (position, velocity, water storage, gas storage...), states of the other aircrafts, position of the buildings, position of the fire, position of the pool, position of the landing position

import math as m
from mpmath import *
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
from scipy.integrate import odeint
from gym.envs.classic_control import rendering

import pyglet


class ContinuousFireDroneEnv(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self):
        self.T = 0.1  # size of the time step before the state is sampled again and new action is given.
        self.num_water_reserviors = 5

        self.min_action_1 = -1.0
        self.max_action_1 = 1.0
        self.min_action_2 = -1.0
        self.max_action_2 = 1.0
        self.min_action_3 = -1.0
        self.max_action_3 = 1.0
        self.min_action_4 = -1.0
        self.max_action_4 = 1.0
        self.min_action_water = -1.0
        self.max_action_water = 1.0
        
        self.min_x_position = -20
        self.max_x_position = 20
        self.min_y_position = -20
        self.max_y_position = 20
        self.min_z_position = 0
        self.max_z_position = 20
        self.min_x_velocity = -20
        self.max_x_velocity = 20
        self.min_y_velocity = -20
        self.max_y_velocity = 20
        self.min_z_velocity = -20
        self.max_z_velocity = 20
        self.min_psi= 0 # yaw
        self.max_psi = 2 * m.pi
        self.min_theta = - m.pi / 2 # pitch
        self.max_theta =   m.pi / 2
        self.min_phi = 0 # roll
        self.max_phi = 2 * m.pi
        self.min_water_storage = 0
        self.max_water_storage = 100
        self.min_gas_storage = 0
        self.max_gas_storage = 10
        
        self.min_d_x = -5 # disturbance mean on different parameters
        self.max_d_x = 5
        self.min_d_y = -5
        self.max_d_y = 5
        self.min_d_z = -5
        self.max_d_z = 5
        self.min_d_vx = -5
        self.max_d_vx = 5
        self.min_d_vy = -5
        self.max_d_vy = 5
        self.min_d_vz = -5
        self.max_d_vz = 5
        self.min_d_phi = - 2 * m.pi
        self.max_d_phi = 2 * m.pi
        self.min_d_theta = - 2 * m.pi
        self.max_d_theta = 2 * m.pi
        self.min_d_psi = - 2 * m.pi
        self.max_d_psi = 2 * m.pi
        
        self.x_1_fire = -20
        self.y_1_fire = -20
        self.x_2_fire = 20
        self.y_2_fire = 20
        self.fire_resolution = 5
        self.min_fire_extent = 0
        self.max_fire_extent = 10

        # will include it in the reward function
        self.num_x_cells = int(abs(self.x_2_fire - self.x_1_fire)/self.fire_resolution)
        self.num_y_cells = int(abs(self.y_2_fire - self.y_1_fire)/self.fire_resolution)
        self.fire_extent = np.zeros((self.num_x_cells, self.num_y_cells))
        
        self.low_state = np.array([self.min_x_position, self.min_y_position, self.min_z_position, self.min_x_velocity,
                                   self.min_y_velocity, self.min_z_velocity, self.min_phi, self.min_theta, self.min_psi,
                                   self.min_water_storage, self.min_gas_storage])

        self.high_state = np.array([self.max_x_position, self.max_y_position, self.max_z_position, self.max_x_velocity,
                                    self.max_y_velocity, self.max_z_velocity, self.max_phi, self.max_theta,
                                    self.max_psi, self.max_water_storage, self.max_gas_storage])
        
        self.min_action = np.array([self.min_action_1, self.min_action_2, self.min_action_3, self.min_action_4,
                                    self.min_action_water])
        self.max_action = np.array([self.max_action_1, self.max_action_2, self.max_action_3, self.max_action_4,
                                    self.max_action_water])
        
        self.viewer = None
        
        self.action_space = spaces.Box(low=self.min_action, high=self.max_action)
        self.observation_space = spaces.Box(low=self.low_state, high=self.high_state)

        self.state = np.zeros(self.low_state.shape)
        self.desired_state = np.zeros(self.low_state.shape)
        self.initial_state = np.zeros(self.low_state.shape)
        self.goal_state = np.zeros(self.low_state.shape)
        self.action = np.zeros(self.min_action.shape)
        self.busy = 0 # a boolean variable saying if I'm targeting some problem
        self.num_itr_to_goal = 0 # number of iterations needed from the time when a goal cell is chosen till reaching the target.
        self.step_num = 0
        self.ang_to_goal = 0

        self.water_reservoirs_pos = np.zeros((self.num_water_reserviors,2))

        self.simstep = self.T/10
        self.Tseq = np.arange(0, self.T, self.simstep)

        self.seed()
        self.reset()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]
    
    def quantize(self,r):
        x = r[0]
        y = r[1]
        if x >= self.max_x_position: x = self.max_x_position - self.fire_resolution/2
        if x <= self.min_x_position: x = self.min_x_position

        if y >= self.max_y_position: y = self.max_y_position - self.fire_resolution/2
        if y <= self.min_y_position: y = self.min_y_position

        xq = int(floor((x-self.x_1_fire)/self.fire_resolution))
        yq = int(floor((y-self.y_1_fire)/self.fire_resolution))
        return xq, yq
    
    def baseline_control(self, s, s_d):
        x = s[0]
        y = s[1]
        z = s[2]
        vx = s[3]
        vy = s[4]
        vz = s[5]
        phi = s[6]
        theta = s[7]
        psi = s[8]
        water = s[9]
        
        x_d = s_d[0]
        y_d = s_d[1]
        z_d = s_d[2]
        vx_d = s_d[3]
        vy_d = s_d[4]
        vz_d = s_d[5]
        phi_d = s_d[6]
        theta_d = s_d[7]
        psi_d = s_d[8]
        #water_d = s_d[9]
        #gas_d = s_d[10]
        
        ax_d = s_d[11]
        ay_d = s_d[12]
        az_d = s_d[13]
        
        phid_d = s_d[14]
        thetad_d = s_d[15]
        psid_d = s_d[16]
        
        Kp = 0.1
        Kp_bar = 0.1
        Kd = 0.1
        
        g = 9.81
        mass = 20

        sd = self.drone_dynamics(s, 1, self.action)
        phid = sd[6]
        thetad = sd[7]
        psid = sd[8]
        
        beta_a = -1*ax_d*m.cos(psi_d) - ay_d*m.sin(psi_d)
        beta_b = -1*az_d + g
        beta_c = -1*ax_d*m.sin(psi_d) + ay_d*m.cos(psi_d)
        
        theta_d = m.atan2(beta_a, beta_b)
        phi_d = m.atan2(beta_c, m.sqrt(beta_a * beta_a + beta_b * beta_b))

        s_d[7] = theta_d
        s_d[8] = phi_d

        '''
        sd_d = self.drone_dynamics(s_d, 1)
        phid_d = (phi_d - phi) / self.T + phid # sd_d[6]
        thetad_d = (theta_d - theta) / self.T + thetad #  sd_d[7]
        psid_d = 0#sd_d[8]
        '''

        phid_d = - phi
        thetad_d = - theta
        psid_d = psi_d - psi
    
        f_ff = - mass * m.sqrt(ax_d * ax_d + ay_d * ay_d + (az_d - g)*(az_d - g))
        w1_ff = phid_d - m.sin(theta_d)*psid_d
        w2_ff = m.cos(phi_d)*thetad_d + m.sin(phi_d)*m.cos(theta_d)*psid_d
        w3_ff = -m.sin(phi_d)*thetad_d + m.cos(phi_d)*m.cos(theta_d)*psid_d

        f_fb = ((m.cos(phi) * m.sin(theta) * m.cos(psi) + m.sin(phi) * m.sin(psi)) * ((x_d - x) * Kp + (vx_d - vx) * Kd) +
                (m.cos(phi) * m.sin(theta) * m.sin(psi) - m.sin(phi) * m.cos(psi)) * ((y_d - y) * Kp + (vy_d - vy) * Kd ) +
                m.cos(phi)*m.cos(theta) * ((z_d - z) * Kp + (vz_d - vz) * Kd))

        w1_fb = Kp * (phi_d - phi) + Kd * (phid_d - phid) + Kp_bar * (y_d - y)
        w2_fb = Kp * (theta_d - theta) + Kd * (thetad_d - thetad) + Kp_bar * (x_d - x)
        w3_fb = Kp * (psi_d - psi) + Kd * (psid_d - psid)

        return [f_ff + f_fb, w1_ff + w1_fb, w2_ff + w2_fb, w3_ff + w3_fb]
    
    def drone_dynamics(self, s, t, a):
        x = s[0]
        y = s[1]
        z = s[2]
        vx = s[3]
        vy = s[4]
        vz = s[5]
        phi = s[6]
        theta = s[7]
        psi = s[8]
        water = s[9]
        gas = s[10]

        fz = a[0] #self.action[0]
        w1 = a[1] #self.action[1]
        w2 = a[2] #self.action[2]
        w3 = a[3] #self.action[3]
        drop = a[4] #self.action[4]
    
        g = 9.81
        mass = 20
        # zw = np.array([[0],[0],[1]])
        # R = np.array([[m.cos(theta)*m.cos(psi), m.sin(phi)*m.sin(theta)*m.cos(psi) - m.cos(phi)*m.sin(psi), m.cos(phi)*m.sin(theta)*m.cos(psi) + m.sin(phi)*m.sin(psi)],
        #     [m.cos(theta)*m.cos(psi), m.sin(phi)*m.sin(theta)*m.cos(psi) + m.cos(phi)*m.sin(psi), m.cos(phi)*m.sin(theta)*m.cos(psi) - m.sin(phi)*m.sin(psi)],
        #     [-1*m.sin(theta), m.sin(phi)*m.cos(theta), m.cos(phi)*m.cos(theta)]])
        #rdd = np.matmul(np.array([g]),zw) + (1/m)*np.matmul(np.matmul(R,zw),np.array([fz]))
    
        xd = vx
        yd = vy
        zd = vz
    
        vxd = (m.cos(phi) * m.sin(theta) * m.cos(psi) + m.sin(phi) * m.sin(psi)) * fz/mass
        vyd = (m.cos(phi) * m.sin(theta) * m.sin(psi) - m.sin(phi) * m.cos(psi)) * fz/mass
        vzd = m.cos(phi) * m.cos(theta) * fz/mass + g

        phid = w1 + m.sin(phi) * m.tan(theta) * w2 + m.cos(phi) * m.tan(theta) * w3
        thetad = m.cos(phi) * w2 - m.sin(phi) * w3
        psid = m.sin(phi) * sec(theta) * w2 + m.cos(phi) * sec(theta) * w3
    
        gasd = -1 * fz  # proportional to thrust
        waterd = -1 * drop  # if drop water then the water reserve will decrease linearly

        # return the state derivatives
        return [xd, yd, zd, vxd, vyd, vzd, phid, thetad, psid, waterd, gasd]

    def plan(self):
        # get the current state of the drone first
        x = self.state[0]
        y = self.state[1]
        z = self.state[2]
        vx = self.state[3]
        vy = self.state[4]
        vz = self.state[5]
        phi = self.state[6]
        theta = self.state[7]
        psi = self.state[8]
        water = self.state[9]
        gas = self.state[10]
        done = 1
        if self.busy != 2 and water <= self.min_water_storage:
            for k in range(self.num_water_reserviors):
                if self.water_extent[k] > self.min_water_storage:
                    i = self.water_reservoirs_pos[k][0]
                    j = self.water_reservoirs_pos[k][1]
                    self.busy = 2
                    done = 0
                    x_d = (i + 1 / 2) * self.fire_resolution + self.x_1_fire
                    y_d = (j + 1 / 2) * self.fire_resolution + self.y_1_fire
                    z_d = 5
                    vx_d = 0
                    vy_d = 0
                    vz_d = 0
                    phi_d = 0
                    theta_d = 0
                    psi_d = 0
                    water_d = 1
                    gas_d = 1
                    self.goal_state = np.array(
                        [x_d, y_d, z_d, vx_d, vy_d, vz_d, phi_d, theta_d, psi_d, water_d, gas_d])
                    #self.initial_state = np.copy(self.state)
                    v1 = [1, 0]
                    v2 = [self.goal_state[0] - x, self.goal_state[1] - y]
                    # self.step_num = 0
                    if v2[0] == 0 and v2[1] == 0:
                        self.ang_to_goal = 0
                    else:
                        self.ang_to_goal = m.acos(
                            (v1[0] * v2[0] + v1[1] * v2[1]) / (m.sqrt(v2[0] * v2[0] + v2[1] * v2[1])))
                    if v2[1] < 0:
                        self.ang_to_goal = 2 * m.pi - self.ang_to_goal
                    break

        if self.busy == 0:  # if not aiming for one fire grid point, search for one that still has fire and aim for it
            for i in range(0, self.num_x_cells):
                if done == 1:
                    for j in range(0, self.num_y_cells):
                        if self.fire_extent[i][j] > 0:
                            self.busy = 1
                            done = 0
                            x_d = (i + 1 / 2) * self.fire_resolution + self.x_1_fire
                            y_d = (j + 1 / 2) * self.fire_resolution + self.y_1_fire
                            z_d = 5
                            vx_d = 0
                            vy_d = 0
                            vz_d = 0
                            phi_d = 0
                            theta_d = 0
                            psi_d = 0
                            water_d = 1
                            gas_d = 1
                            self.goal_state = np.array(
                                [x_d, y_d, z_d, vx_d, vy_d, vz_d, phi_d, theta_d, psi_d, water_d, gas_d])
                            #self.initial_state = np.copy(self.state)
                            v1 = [1, 0]
                            v2 = [self.goal_state[0] - x, self.goal_state[1] - y]
                            #self.step_num = 0
                            if v2[0] == 0 and v2[1] == 0:
                                self.ang_to_goal = 0
                            else:
                                self.ang_to_goal = m.acos(
                                    (v1[0] * v2[0] + v1[1] * v2[1]) / (m.sqrt(v2[0] * v2[0] + v2[1] * v2[1])))
                            if v2[1] < 0:
                                self.ang_to_goal = 2 * m.pi - self.ang_to_goal
                            '''
                            self.state[8] = self.ang_to_goal + m.pi/2# assume that the drone is directed toward the goal for now.
                            self.state[6] = 0
                            self.state[7] = 0
                            self.state[3] = m.cos(self.ang_to_goal)
                            self.state[4] = m.sin(self.ang_to_goal)
                            self.state[5] = 0
                            '''
                            break
                else:
                    break

        # the direction of the goal, the direction we want the drone to move in
        v1 = [1, 0]
        v2 = [self.goal_state[0] - x, self.goal_state[1] - y]
        dist_to_dest = m.sqrt(v2[0] * v2[0] + v2[1] * v2[1])

        # first get the angle of the goal
        if v2[0] == 0 and v2[1] == 0:
            self.ang_to_goal = 0
        else:
            self.ang_to_goal = m.acos((v1[0] * v2[0] + v1[1] * v2[1]) / dist_to_dest)
        if v2[1] < 0:
            self.ang_to_goal = 2 * m.pi - self.ang_to_goal

        x_d = m.cos(self.ang_to_goal) * self.T + x  # * self.step_num + self.initial_state[0] # m.cos(ang) * (self.T * self.T + self.T)  + x
        y_d = m.sin(self.ang_to_goal) * self.T + y  # * self.step_num + self.initial_state[1] # m.sin(ang) * (self.T * self.T + self.T) + y
        vx_d = 2 * m.cos(self.ang_to_goal)  # (m.cos(ang) - vz) / self.T + vx
        vy_d =  2 * m.sin(self.ang_to_goal)  # (m.sin(ang) - vy) / self.T + vy

        z_d = (5 - z) * self.T + z
        vz_d = (5 - z)
        phi_d = 100#(0 - phi) * self.T + phi
        theta_d = 100 #(0 - theta) * self.T + theta
        psi_d = self.ang_to_goal + m.pi / 2 #(self.ang_to_goal + m.pi / 2 - psi) * self.T + psi  # (ang + m.pi / 2 - psi) / self.T + psi
        water_d = 0
        gas_d = 0
        ax_d = vx_d - vx  # m.cos(ang)
        ay_d = vy_d - vy  # m.sin(ang)
        az_d = vz_d - vz
        phid_d = phi_d - phi #0  # (phi_d - phi) / self.T
        thetad_d = theta_d - theta #0  # (theta_d - theta) / self.T
        psid_d = psi_d - psi #0  # (psi_d - psi) / self.T
        self.desired_state = np.array(
            [x_d, y_d, z_d, vx_d, vy_d, vz_d, phi_d, theta_d, psi_d, water_d, gas_d, ax_d, ay_d, az_d,
             phid_d, thetad_d, psid_d])
        self.action[0:4] = self.baseline_control(self.state, self.desired_state)
        if (x >= self.goal_state[0] - self.fire_resolution/2) and (x <= self.goal_state[0] + self.fire_resolution/2)\
                and (y >= self.goal_state[1]  - self.fire_resolution/2) and (y <= self.goal_state[1] + self.fire_resolution/2):
            if self.busy == 1:
                self.action[4] = 1  # drop water
            elif self.busy == 2:
                self.action[4] = -1
            else:
                self.action[4] = 0
        else:
            self.action[4] = 0
        return self.action

    def step(self, a):
        print("state: ", self.state)
        water_before = self.state[9]
        new_state = odeint(self.drone_dynamics, self.state, self.Tseq, args= tuple([a]))
        #self.step_num = self.step_num + 1
        self.state[0:11] = new_state[-1, :]
        x = self.state[0]
        y = self.state[1]
        z = self.state[2]
        vx = self.state[3]
        vy = self.state[4]
        vz = self.state[5]
        phi = self.state[6]
        theta = self.state[7]
        psi = self.state[8]
        water = self.state[9]
        gas = self.state[10]

        drop = a[4]
        
        if vx > self.max_x_velocity: vx = self.max_x_velocity
        if vx < self.min_x_velocity: vx = self.min_x_velocity
        
        if vy > self.max_y_velocity: vx = self.max_y_velocity
        if vy < self.min_y_velocity: vx = self.min_y_velocity
        
        if vz > self.max_z_velocity: vx = self.max_z_velocity
        if vz < self.min_z_velocity: vx = self.min_z_velocity
        
        if x > self.max_x_position: x = self.max_x_position
        if x < self.min_x_position: x = self.min_x_position
        
        if y > self.max_y_position: y = self.max_y_position
        if y < self.min_y_position: y = self.min_y_position
        
        if z > self.max_z_position: z = self.max_z_position
        if z < self.min_z_position: z = self.min_z_position
        
        if x == self.min_x_position and vx < 0: vx = 0
        if y == self.min_y_position and vy < 0: vy = 0
        if z == self.min_z_position and vz < 0: vz = 0
        
        if x == self.max_x_position and vx > 0: vx = 0
        if y == self.max_y_position and vy > 0: vy = 0
        if z == self.max_z_position and vz > 0: vz = 0

        if water < self.min_water_storage: water = self.min_water_storage
        if water > self.max_water_storage: water = self.max_water_storage

        self.state[0] = x
        self.state[1] = y
        self.state[2] = z
        self.state[3] = vx
        self.state[4] = vy
        self.state[5] = vz
        self.state[6] = phi
        self.state[7] = theta
        self.state[8] = psi
        self.state[9] = water
        self.state[10] = gas
        
        
        reward = 0
        # I'm ignoring the gas and water loss for now.
        for i in range(0, new_state.shape[0]):
            rq = self.quantize(new_state[i][0:2])
            if drop >=0 and water > self.min_water_storage:
                new_fire_extent = max(0, self.fire_extent[rq[0]][rq[1]] - max(water_before - new_state[i][9], 0)) # the decrease in fire extent
                reward = reward + new_fire_extent - self.fire_extent[rq[0]][rq[1]]
                self.fire_extent[rq[0]][rq[1]] = new_fire_extent
                water_before = new_state[i][9]
            else:
                res_index = self.is_water_reservior(rq[0], rq[1])
                if drop < 0 and  res_index >= 0:
                    #self.state[9] = min(self.max_water_storage, self.state[9] - drop * self.simstep)
                    self.water_extent[res_index] = max(0, self.water_extent[res_index] - new_state[i][9] + water_before)
                    water_before = new_state[i][9]

        if self.busy == 1:
            rq = self.quantize(self.goal_state[0:2])
            if self.fire_extent[rq[0]][rq[1]] <= 0:
                self.busy = 0

        if self.busy == 2:
            rq = self.quantize(self.goal_state[0:2])
            if self.water_extent[self.is_water_reservior(rq[0],rq[1])] <= self.min_water_storage or water >= self.max_water_storage:
                self.busy = 0
        
        done = 1
        for i in range(0,self.num_x_cells):
            if done == 1:
                for j in range(0,self.num_y_cells):
                    if self.fire_extent[i][j] > 0:
                        done = 0
                        break
        if done:
            reward = 100.0
        
        #reward-= math.pow(action[0],2)*0.1
        return self.state, reward, done, {}

    def is_water_reservior(self, i, j):
        for k in range(self.num_water_reserviors):
            if self.water_reservoirs_pos[k][0] == i and self.water_reservoirs_pos[k][1] == j:
                return k
        return -1

    def reset(self):
        self.state = np.array([0,
                               0,
                               5, # fix it for now at 5
                               0, #self.np_random.uniform(low=self.min_x_velocity, high=self.max_x_velocity),
                               0, #self.np_random.uniform(low=self.min_y_velocity, high=self.max_y_velocity),
                               0, #self.np_random.uniform(low=self.min_z_velocity, high=self.max_z_velocity),
                               0, #self.np_random.uniform(low=self.min_phi, high=self.max_phi),
                               0, #self.np_random.uniform(low=self.min_theta, high=self.max_theta),
                               0, #self.np_random.uniform(low=self.min_psi, high=self.max_psi),
                               self.np_random.uniform(low=self.min_water_storage, high=self.max_water_storage),
                               self.np_random.uniform(low=self.min_gas_storage, high=self.max_gas_storage)])

        self.goal_state = np.array([0,0,0,0,0,0,0,0,0,0,0])

        self.num_itr_to_goal = 0
        self.ang_to_goal = 0

        self.fire_extent = self.max_fire_extent * np.random.rand(self.num_x_cells, self.num_y_cells)

        water_reservoirs_pos_x = self.np_random.randint(0, self.num_x_cells,
                                                        size=self.num_water_reserviors)
        water_reservoirs_pos_y = self.np_random.randint(0, self.num_y_cells,
                                                        size=self.num_water_reserviors)
        self.water_reservoirs_pos = np.transpose(
            np.concatenate([[water_reservoirs_pos_x], [water_reservoirs_pos_y]]))

        self.water_extent = self.max_water_storage * np.random.rand(self.num_water_reserviors)

        # making sure that there is no fire at the same cells with water reserviors
        for i in range(0, self.num_water_reserviors):
            self.fire_extent[self.water_reservoirs_pos[i][0]][self.water_reservoirs_pos[i][1]] = 0

        self.busy = 0 # a variable specifying if the drone is currently following some target
        #self.step_num = 0
        return np.array(self.state)

    def render(self, mode='human'):
        map_width = 600
        state_display_width = 200
        screen_width = map_width + state_display_width
        screen_height = 600

        world_width = self.max_x_position - self.min_x_position
        scale = map_width / world_width
        dronewidth = 10
        droneheight = 10

        if self.viewer is None:

            self.viewer = rendering.Viewer(screen_width, screen_height)

            for i in range(0, self.num_x_cells+1):
                xs = (i * self.fire_resolution + self.min_x_position) * np.ones(100)
                ys = np.linspace(self.min_y_position, self.max_y_position, 100)
                xys = list(zip((xs - self.min_x_position) * scale, (ys - self.min_y_position) * scale))
                self.track = rendering.make_polyline(xys)
                self.track.set_linewidth(2)
                self.viewer.add_geom(self.track)
            for i in range(0, self.num_y_cells+1):
                xs = np.linspace(self.min_x_position, self.max_x_position, 100)
                ys = (i * self.fire_resolution + self.min_y_position) * np.ones(100)
                xys = list(zip((xs - self.min_x_position) * scale, (ys - self.min_y_position) * scale))
                self.track = rendering.make_polyline(xys)
                self.track.set_linewidth(2)
                self.viewer.add_geom(self.track)

            l, r, t, b = -dronewidth / 2, dronewidth / 2, -droneheight/2, droneheight/2
            drone = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            drone.add_attr(rendering.Transform(translation=(self.state[0], self.state[1])))
            self.dronetrans = rendering.Transform()
            drone.add_attr(self.dronetrans)
            self.viewer.add_geom(drone)

            l, r, t, b = -dronewidth / 8, dronewidth / 8, droneheight * 2, 0
            psirod = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            psirod.add_attr(rendering.Transform(translation=(self.state[0], self.state[1])))
            self.psirodtrans = rendering.Transform()
            psirod.add_attr(self.psirodtrans)
            self.viewer.add_geom(psirod)

            phirod = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            phirod.set_color(0, 0, 0.8)
            phirod.add_attr(rendering.Transform(translation=(self.state[0], self.state[1])))
            self.phirodtrans = rendering.Transform()
            phirod.add_attr(self.phirodtrans)
            self.viewer.add_geom(phirod)

            thetarod = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            thetarod.set_color(0, 0.8, 0)
            thetarod.add_attr(rendering.Transform(translation=(self.state[0], self.state[1])))
            self.thetarodtrans = rendering.Transform()
            thetarod.add_attr(self.thetarodtrans)
            self.viewer.add_geom(thetarod)

            angrod = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            angrod.set_color(0.8, 0, 0)
            angrod.add_attr(rendering.Transform(translation=(self.state[0], self.state[1])))
            self.angrodtrans = rendering.Transform()
            angrod.add_attr(self.angrodtrans)
            self.viewer.add_geom(angrod)

        label = pyglet.text.Label('Hello, world',
                                  font_name='Times New Roman',
                                  font_size=36,
                                  x=self.viewer.window.width // 2, y=self.viewer.window.height // 2,
                                  anchor_x='center', anchor_y='center')

        label.draw()

        @self.viewer.window.event
        def on_draw():
            self.viewer.window.clear()
            label.draw()

        #pyglet.app.run()

        flagx = (self.goal_state[0] - self.min_x_position) * scale
        flagy1 = (self.goal_state[1]  - self.min_y_position) * scale
        flagy2 = flagy1 + self.fire_resolution/4 * scale
        flagpole = rendering.Line((flagx, flagy1), (flagx, flagy2))
        flagpole.set_color(0, .8, 0)
        self.viewer.add_onetime(flagpole)
        flag = rendering.FilledPolygon([(flagx, flagy2), (flagx, flagy2 - self.fire_resolution/10 * scale),
                                        (flagx + self.fire_resolution/6 * scale, flagy2 - self.fire_resolution/15 * scale)])
        flag.set_color(.8, .8, 0)
        self.viewer.add_onetime(flag)

        desired = rendering.make_circle(droneheight / 2.5)
        desired.set_color(.5, .5, .5)
        desired.add_attr(rendering.Transform(translation=( (self.desired_state[0] - self.min_x_position) * scale, (self.desired_state[1] - self.min_y_position) * scale)))
        self.viewer.add_onetime(desired)

        for i in range(0, self.num_x_cells):
            for j in range(0, self.num_y_cells):
                if i != self.num_x_cells or j != self.num_y_cells:
                    if self.fire_extent[i][j] > 0:
                        fire = rendering.make_circle((self.fire_extent[i][j] / self.max_fire_extent) * self.fire_resolution)
                        fire.add_attr(rendering.Transform(translation=(
                        (i + 1 / 2) * self.fire_resolution * scale, (j + 1 / 2) * self.fire_resolution * scale)))
                        #fire.set_color(10, 0, 0)
                        fire._color.vec4 = (10, 0, 0, 1)
                        self.viewer.add_onetime(fire)

        for k in range(0, self.num_water_reserviors):
            i = self.water_reservoirs_pos[k][0]
            j = self.water_reservoirs_pos[k][1]
            water = rendering.make_circle((self.water_extent[k] / self.max_water_storage) * self.fire_resolution)
            water.add_attr(rendering.Transform(translation=(
            (i + 1 / 2) * self.fire_resolution * scale, (j + 1 / 2) * self.fire_resolution * scale)))
            water.set_color(0, 0, 0.8)
            self.viewer.add_onetime(water)



        x_pos = self.state[0]
        y_pos = self.state[1]
        self.dronetrans.set_translation((x_pos - self.min_x_position) * scale, (y_pos - self.min_y_position) * scale)

        self.psirodtrans.set_translation((x_pos - self.min_x_position) * scale, (y_pos - self.min_y_position) * scale)
        self.psirodtrans.set_rotation(self.state[8] - m.pi/2)

        self.phirodtrans.set_translation((x_pos - self.min_x_position) * scale, (y_pos - self.min_y_position) * scale)
        self.phirodtrans.set_rotation(self.state[6] - m.pi/2)

        self.thetarodtrans.set_translation((x_pos - self.min_x_position) * scale, (y_pos - self.min_y_position) * scale)
        self.thetarodtrans.set_rotation(self.state[7] - m.pi/2)

        self.angrodtrans.set_translation((x_pos - self.min_x_position) * scale, (y_pos - self.min_y_position) * scale)
        self.angrodtrans.set_rotation(self.ang_to_goal - m.pi / 2)

        return self.viewer.render(return_rgb_array=mode == 'rgb_array')

    def close(self):
        if self.viewer: self.viewer.close()

