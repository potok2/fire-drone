import numpy as np
import sys
from gym.envs.toy_text import discrete

UP_m = 0
RIGHT_m = 1
DOWN_m = 2
LEFT_m = 3

UP_o = 4
RIGHT_o = 5
DOWN_o = 6
LEFT_o = 7

M = 4
N = 4 # grid width (and length for now)
#K = 81

B = 2 # number of times (MINUS one) the agent can sense one location -- this constraint is motivated by timing constraint before taking an action (autonomous vehicle should move, not just stop in the middle of the street and sense :p)
# it is named B because it will be the base in which we represent the state.

prior_p = 0.5 #prior probability that a cell is occupied.



def Bary (n): # the name is inspired by ternary for three, so B -> Bary :p
    if n == 0:
        return "00000000"
    nums = []
    while n:
        n, r = divmod(n, B)
        nums.append(str(r))
    s = ''.join(reversed(nums))
    while len(s)<8:
        s = "0" + s
    return s

def inv_Bary(s):
    n = 0
    base_mult = 1
    if len(s)==0:
        return n*1
    l = len(s)
    for i in range(0,l):
        n = n + (ord(s[l - i - 1]) - ord('0')) * base_mult
        #print("blabla = ", ord(s[l - i - 1]) - ord('0'))
        base_mult = B * base_mult
    return n
'''
def dir_blocked(dir, i_t):
    if dir == UP_m:
        if (i_t[0]=='0' and i_t[1]=='1'):
            return True
        else
            return False
    if dir == RIGHT_m:
        if (i_t[2]=='0' and i_t[3]=='1'):
            return True
        else
            return False
    if dir == DOWN_m:
        if (i_t[4]=='0' and i_t[5]=='1'):
            return True
        else
            return False
    if dir == LEFT_m:
        if (i_t[6]=='0' and i_t[7]=='1'):
            return True
        else
            return False
    return False
'''

def not_state(i_t):
    not_true_state = False
    for j in range(0,len(i_t)):
        if j % 2 == 0 and ord(i_t[j]) < ord(i_t[j+1]):# and not dir_blocked(int(j/2),i_t): # in that case it is not an actual state.
            not_true_state = True
            break
    return not_true_state


# "01" at the j and j+1 locations in i_t means that we are sure that the up direction is blocked. This can be achieved by taking an action in that direction and couldn't move

class GridworldEnv(discrete.DiscreteEnv):
    """
        Grid World environment from Sutton's Reinforcement Learning book chapter 4.
        You are an agent on an MxN grid and your goal is to reach the terminal
        state at the top left or the bottom right corner.
        
        For example, a 4x4 grid looks as follows:
        
        T  o  o  o
        o  x  o  o
        o  o  o  o
        o  o  o  T
        
        x is your position and T are the two terminal states.
        
        You can take actions in each direction (UP=0, RIGHT=1, DOWN=2, LEFT=3).
        Actions going off the edge leave you in your current state.
        You receive a reward of -1 at each step until you reach a terminal state.
        """
    
    metadata = {'render.modes': ['human', 'ansi']}
    
    def __init__(self, shape=[M,N]):
        if not isinstance(shape, (list, tuple)) or not len(shape) == 2:
            raise ValueError('shape argument must be a list/tuple of length 2')
    
        self.shape = shape
        K = B**8
        self.K = K
        
        nS = np.prod(shape) * K # this is fake number of states. The number of states is actually smaller
        # for example, there cannot 2 sensing actions and the number of correct sensing is 3
        nG = np.prod(shape) #the number of cells in the grid
        nA = 4*2
        
        
        MAX_Y = shape[0]
        MAX_X = shape[1]
        
        P = {}
        grid = np.arange(nG).reshape(shape) # the index of the cells
        grid_f = np.random.rand(1, M * N) # the availability of the cell
        grid_f = [round(val) for val in grid_f[0,:]]
        
        it = np.nditer(grid, flags=['multi_index'])
        
        while not it.finished:
            y, x = it.multi_index
            si = it.iterindex * K # the cell index
            ns_grid = int(si/K)
            sensors_failure_prob = 0.5 * np.random.rand(1,4)
            rewards_temp =  np.random.rand(1,8)
            rewards = [round(val*10) for val in rewards_temp[0,:]]
            for j in range(4):
                rewards[j] = rewards[j]*10 # penalize hitting an obstacle more than sensing an obstacle
            
            for i in range(0,K):
                s = si + i
                i_t = Bary(i)
                if not_state(i_t):
                    continue
            
                P[s] = {a : [] for a in range(nA)}
            
                #is_done = lambda s: s == 0 or s == nG - 1
                is_done = lambda s: s == nG - 1
                #reward = 0.0 if is_done(s) else -1.0
                
                finishing_reward = 20
                # We're stuck in a terminal state
                if is_done(ns_grid):
                    P[s][UP_m] = [(1.0, s, 0, True)]
                    P[s][RIGHT_m] = [(1.0, s, 0, True)]
                    P[s][DOWN_m] = [(1.0, s, 0, True)]
                    P[s][LEFT_m] = [(1.0, s, 0, True)]
                    P[s][UP_o] = [(1.0, s, 0, True)]
                    P[s][RIGHT_o] = [(1.0, s, 0, True)]
                    P[s][DOWN_o] = [(1.0, s, 0, True)]
                    P[s][LEFT_o] = [(1.0, s, 0, True)]
                # Not a terminal state
                else:
                    ### These are the states with for the states with no sensing of the neighborhood yet.
                    ns_up = si if y == 0 else si - MAX_X * K
                    ns_right = si if x == (MAX_X - 1) else si + K
                    ns_down = si if y == (MAX_Y - 1) else si + MAX_X * K
                    ns_left = si if x == 0 else si - K
                    
                    ### These are the indices of the new cells in the grid after the different actions
                    ns_up_grid = int(ns_up/K)
                    ns_right_grid = int(ns_right/K)
                    ns_left_grid = int(ns_left/K)
                    ns_down_grid = int(ns_down/K)
                    
                    temp_p_a = ((1 - sensors_failure_prob[0][0]) ** (ord(i_t[1]) - ord('0'))) * ((sensors_failure_prob[0][0]) ** (ord(i_t[0]) - ord(i_t[1])))
                    temp_p_b = ((1 - sensors_failure_prob[0][0]) ** (ord(i_t[0]) - ord(i_t[1]))) * ((sensors_failure_prob[0][0]) ** (ord(i_t[1]) - ord('0')))
                    prob_up_full = temp_p_a * prior_p / ( temp_p_a * prior_p + temp_p_b * (1 - prior_p)) # probability that the up cell is full given the observation (which is in this case the state)
                    prob_up_free = 1 - prob_up_full
                    
                    
                    if y > 0: # and grid_f[ns_up_grid] == 0: # you may be able to go up
                        P[s][UP_m] = [(prob_up_free, ns_up, finishing_reward if is_done(ns_up_grid) else 0, is_done(ns_up_grid)), (prob_up_full, si,  -1*rewards[0], False)]
                    elif y == 0: # can't go up
                        P[s][UP_m] = [(1.0, si, -1*rewards[0], False)] # for sure, the up direction is blocked
                    
                    temp_p_a = ((1 - sensors_failure_prob[0][1]) ** (ord(i_t[3]) - ord('0'))) * ((sensors_failure_prob[0][1]) ** (ord(i_t[2]) - ord(i_t[3])))
                    temp_p_b = ((1 - sensors_failure_prob[0][1]) ** (ord(i_t[2]) - ord(i_t[3]))) * ((sensors_failure_prob[0][1]) ** (ord(i_t[3]) - ord('0')))
                    prob_right_full = temp_p_a * prior_p / ( temp_p_a * prior_p + temp_p_b * (1 - prior_p)) # probability that the up cell is full given the observation (which is in this case the state)
                    prob_right_free = 1 - prob_right_full
                    
                    if x < (MAX_X - 1): # you may be able to go right
                        P[s][RIGHT_m] = [(prob_right_free, ns_right, finishing_reward if is_done(ns_right_grid) else 0, is_done(ns_right_grid)), (prob_right_full, si, -1*rewards[1], False)]
                    elif x == (MAX_X - 1): # can't go right
                        P[s][RIGHT_m] = [(1.0, si, -1*rewards[1],False)]
                    
                    temp_p_a = ((1 - sensors_failure_prob[0][2]) ** (ord(i_t[5]) - ord('0'))) * ((sensors_failure_prob[0][2]) ** (ord(i_t[4]) - ord(i_t[5])))
                    temp_p_b = ((1 - sensors_failure_prob[0][2]) ** (ord(i_t[4]) - ord(i_t[5]))) * ((sensors_failure_prob[0][2]) ** (ord(i_t[5]) - ord('0')))
                    prob_down_full = temp_p_a * prior_p / ( temp_p_a * prior_p + temp_p_b * (1 - prior_p)) # probability that the up cell is full given the observation (which is in this case the state)
                    prob_down_free = 1 - prob_down_full
                    
                    if y < (MAX_Y - 1): # you may be able to go down
                        P[s][DOWN_m] = [(prob_down_free, ns_down, finishing_reward if is_done(ns_down_grid) else 0, is_done(ns_down_grid)), (prob_down_full, si, -1*rewards[2], False)]
                    elif y == (MAX_Y - 1): # can't go down
                        P[s][DOWN_m] = [(1.0, si, -1*rewards[2], False)]
                    
                    temp_p_a = ((1 - sensors_failure_prob[0][3]) ** (ord(i_t[7]) - ord('0'))) * ((sensors_failure_prob[0][3]) ** (ord(i_t[6]) - ord(i_t[7])))
                    temp_p_b = ((1 - sensors_failure_prob[0][3]) ** (ord(i_t[6]) - ord(i_t[7]))) * ((sensors_failure_prob[0][3]) ** (ord(i_t[7]) - ord('0')))
                    prob_left_full = temp_p_a * prior_p / ( temp_p_a * prior_p + temp_p_b * (1 - prior_p)) # probability that the up cell is full given the observation (which is in this case the state)
                    prob_left_free = 1 - prob_left_full
                    
                    if x > 0: # you may be able to go left
                        P[s][LEFT_m] = [(prob_left_free, ns_left, finishing_reward if is_done(ns_left_grid) else 0, is_done(ns_left_grid)), (prob_left_full, si, -1*rewards[3], False)]
                    elif x == 0: # can't go left
                        P[s][LEFT_m] = [(1.0, si, -1*rewards[3], False)]
                    
                    num_sense_up = ord(i_t[0]) - ord('0') # number of times the up direction is sensed.
                    num_sense_up_c = ord(i_t[1]) # number of times it was correctly sensed
                    num_sense_right = ord(i_t[2]) - ord('0') # similar but for right direction
                    num_sense_right_c = ord(i_t[3])
                    num_sense_down = ord(i_t[4]) - ord('0') # similar but for down direction
                    num_sense_down_c = ord(i_t[5])
                    num_sense_left = ord(i_t[6]) - ord('0') # similar but for left direction
                    num_sense_left_c = ord(i_t[7])
                    
                    p_o_up_full =  (1 - sensors_failure_prob[0][0]) * prob_up_full +  (sensors_failure_prob[0][0]) * prob_up_free # probability that we observe that the up cell is full given the history of observations
                    p_o_up_free = 1 - p_o_up_full
                    
                    p_o_right_full =  (1 - sensors_failure_prob[0][1]) * prob_right_full +  (sensors_failure_prob[0][1]) * prob_right_free # probability that we observe that the right cell is full given the history of observations
                    p_o_right_free = 1 - p_o_right_full
                    
                    p_o_down_full =  (1 - sensors_failure_prob[0][2]) * prob_down_full +  (sensors_failure_prob[0][2]) * prob_down_free # probability that we observe that the down cell is full given the history of observations
                    p_o_down_free = 1 - p_o_down_full
                    
                    p_o_left_full =  (1 - sensors_failure_prob[0][3]) * prob_left_full +  (sensors_failure_prob[0][3]) * prob_left_free # probability that we observe that the down cell is full given the history of observations
                    p_o_left_free = 1 - p_o_left_full
                    
                    new_i_t_a = ""
                    new_i_t_b = ""
                    if num_sense_up < B - 1:
                        num_sense_up = chr(num_sense_up + 1 + ord('0'))
                        temp = chr(num_sense_up_c + 1)
                        new_i_t_a = num_sense_up + temp + i_t[2:] # if it sensed that is full
                        new_i_t_b = num_sense_up + i_t[1:] # if it sensed that is free
                        if  y > 0:
                            P[s][UP_o] = [(p_o_up_full, si + inv_Bary(new_i_t_a), -1*rewards[4], False), (p_o_up_free, si + inv_Bary(new_i_t_b), -1*rewards[4], False)]
                        elif y == 0:
                            P[s][UP_o] = [(1.0, si + inv_Bary(new_i_t_b), -1*rewards[4], False)]
                    else:
                        P[s][UP_o] = [(1.0, s, -1*rewards[4], False)] # don't sense the same cell more than B - 1 times
                
                    if num_sense_right < B - 1:
                        num_sense_right = chr(num_sense_right + 1 + ord('0'))
                        temp = chr(num_sense_right_c + 1)
                        new_i_t_a = i_t[0:2] + num_sense_right + temp + i_t[4:] # if it sensed that is full
                        new_i_t_b = i_t[0:2] + num_sense_right + i_t[3:] # if it sensed that is free
                        if x < (MAX_X - 1):
                            P[s][RIGHT_o] = [(p_o_right_full, si +  inv_Bary(new_i_t_a), -1*rewards[5], False), ( p_o_right_free, si +  inv_Bary(new_i_t_b), -1*rewards[5], False)]
                        elif x == (MAX_X - 1):
                            P[s][RIGHT_o] = [(1.0, si + inv_Bary(new_i_t_b), -1*rewards[5], False)]
                    else:
                        P[s][RIGHT_o] = [(1.0, s, -1*rewards[5], False)] # don't sense the same cell more than B - 1 times
                    
                    if num_sense_down < B - 1:
                        num_sense_down = chr(num_sense_down + 1 + ord('0'))
                        temp = chr(num_sense_down_c + 1)
                        new_i_t_a = i_t[0:4] + num_sense_down + temp + i_t[6:] # if it sensed that is full
                        new_i_t_b = i_t[0:4] + num_sense_down + i_t[5:] # if it sensed that is free
                        if y < (MAX_Y - 1):
                            P[s][DOWN_o] = [(p_o_down_full, si + inv_Bary(new_i_t_a), -1*rewards[6], False), (p_o_down_free, si + inv_Bary(new_i_t_b), -1*rewards[6], False)]
                        elif y == (MAX_Y - 1):
                            P[s][DOWN_o] = [(1.0, si + inv_Bary(new_i_t_b), -1*rewards[6], False)]
                    else:
                        P[s][DOWN_o] = [(1.0, s, -1*rewards[6], False)] # don't sense the same cell more than B - 1 times

                    if num_sense_left < B - 1:
                        num_sense_left = chr(num_sense_left + 1 + ord('0'))
                        temp = chr(num_sense_left_c + 1)
                        new_i_t_a = i_t[0:6] + num_sense_left + temp # if it sensed that is full
                        new_i_t_b = i_t[0:6] + num_sense_left + i_t[7] # if it sensed that is free
                        if x > 0:
                            P[s][LEFT_o] = [(p_o_left_full, si +  inv_Bary(new_i_t_a), -1*rewards[7], False), (p_o_left_free, si +  inv_Bary(new_i_t_b), -1*rewards[7], False)]
                        elif x == 0:
                            P[s][LEFT_o] = [(1.0, si + inv_Bary(new_i_t_b), -1*rewards[7], False)]
                    else:
                        P[s][LEFT_o] = [(1.0, s, -1*rewards[7], False)] # don't sense the same cell more than B times
        
            it.iternext()
        
        # Initial state distribution is uniform
        #isd = np.ones(nS) / nS
        isd = np.zeros(nS)
        isd[0] = 1
        
        # We expose the model of the environment for educational purposes
        # This should not be used in any model-free learning algorithm
        self.P = P
        
        super(GridworldEnv, self).__init__(nS, nA, P, isd)

    def _render(self, mode='human', close=False):
        if close:
            return
        
        outfile = StringIO() if mode == 'ansi' else sys.stdout
        
        grid = np.arange(self.nS).reshape(self.shape)
        it = np.nditer(grid, flags=['multi_index'])
        while not it.finished:
            si = it.iterindex
            y, x = it.multi_index
        
            if floor(self.s/K)*K == si:
                output = " x "
    
            elif s == 0 or s == self.nS - 1:
                output = " T "
            else:
                    output = " o "

            if x == 0:
                output = output.lstrip()
            if x == self.shape[1] - 1:
                output = output.rstrip()

            outfile.write(output)
    
            if x == self.shape[1] - 1:
                outfile.write("\n")
            
            it.iternext()