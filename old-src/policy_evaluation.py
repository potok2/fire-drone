import numpy as np
import pprint
import sys
if "../" not in sys.path:
    sys.path.append("../")

from modified_gridworld_new import GridworldEnv

pp = pprint.PrettyPrinter(indent=2)
env = GridworldEnv()

B = 2 # number of times (MINUS one) the agent can sense one location -- this constraint is motivated by timing constraint before taking an action (autonomous vehicle should move, not just stop in the middle of the street and sense :p)
# it is named B because it will be the base in which we represent the state.



def Bary (n): # the name is inspired by ternary for three, so B -> Bary :p
    if n == 0:
        return "00000000"
    nums = []
    while n:
        n, r = divmod(n, B)
        nums.append(str(r))
    s = ''.join(reversed(nums))
    while len(s)<8:
        s = "0" + s
    return s

def inv_Bary(s):
    n = 0
    base_mult = 1
    if len(s)==0:
        return n*1
    l = len(s)
    for i in range(0,l):
        n = n + (ord(s[l - i - 1]) - ord('0')) * base_mult
        #print("blabla = ", ord(s[l - i - 1]) - ord('0'))
        base_mult = B * base_mult
    return n

def not_state(i_t):
    not_true_state = False
    for j in range(0,len(i_t)):
        if j % 2 == 0  and ord(i_t[j]) < ord(i_t[j+1]):# and not dir_blocked(int(j/2),i_t): # in that case it is not an actual state.
            not_true_state = True
            break
    return not_true_state

def policy_eval(policy, env, discount_factor=0.9, theta=0.00001):
    """
        Evaluate a policy given an environment and a full description of the environment's dynamics.
        
        Args:
        policy: [S, A] shaped matrix representing the policy.
        env: OpenAI env. env.P represents the transition probabilities of the environment.
        env.P[s][a] is a list of transition tuples (prob, next_state, reward, done).
        env.nS is a number of states in the environment.
        env.nA is a number of actions in the environment.
        theta: We stop evaluation once our value function change is less than theta for all states.
        discount_factor: Gamma discount factor.
        
        Returns:
        Vector of length env.nS representing the value function.
        """
    # Start with a random (all 0) value function
    V = np.zeros(env.nS)
    K = env.K
    while True:
        delta = 0
        # For each state, perform a "full backup"
        for s in range(env.nS):
            v = 0
            if not_state(Bary(s%K)):
                continue
            #print("eval s:",s)
            # Look at the possible next actions
            for a, action_prob in enumerate(policy[s]):
                # For each action, look at the possible next states...
                for  prob, next_state, reward, done in env.P[s][a]:
                    # Calculate the expected value
                    v += action_prob * prob * (reward + discount_factor * V[next_state])
            # How much our value function changed (across any states)
            delta = max(delta, np.abs(v - V[s]))
            V[s] = v
        # Stop evaluating once our value function change is below a threshold
        if delta < theta:
            break
    return np.array(V)

def policy_improvement(env, policy_eval_fn=policy_eval, discount_factor=0.9):
    """
        Policy Improvement Algorithm. Iteratively evaluates and improves a policy
        until an optimal policy is found.
        
        Args:
        env: The OpenAI envrionment.
        policy_eval_fn: Policy Evaluation function that takes 3 arguments:
        policy, env, discount_factor.
        discount_factor: gamma discount factor.
        
        Returns:
        A tuple (policy, V).
        policy is the optimal policy, a matrix of shape [S, A] where each state s
        contains a valid probability distribution over actions.
        V is the value function for the optimal policy.
        
        """
    # Start with a random policy
    policy = np.ones([env.nS, env.nA]) / env.nA
    K = env.K
    while True:
        # Evaluate the current policy
        V = policy_eval_fn(policy, env, discount_factor)
        
        # Will be set to false if we make any changes to the policy
        policy_stable = True
        
        # For each state...
        for s in range(env.nS):
            if not_state(Bary(s%K)):
                continue
            # The best action we would take under the currect policy
            chosen_a = np.argmax(policy[s])
            
            # Find the best action by one-step lookahead
            # Ties are resolved arbitarily
            action_values = np.zeros(env.nA)
            for a in range(env.nA):
                for prob, next_state, reward, done in env.P[s][a]:
                    action_values[a] += prob * (reward + discount_factor * V[next_state])
            best_a = np.argmax(action_values)
            
            # Greedily update the policy
            if chosen_a != best_a:
                policy_stable = False
            policy[s] = np.eye(env.nA)[best_a]
        
        # If the policy is stable we've found an optimal policy. Return it
        if policy_stable:
            return policy, V


def value_iteration(env, theta=0.0001, discount_factor=0.9):
    """
        Value Iteration Algorithm.
        
        Args:
        env: OpenAI env. env.P represents the transition probabilities of the environment.
        env.P[s][a] is a list of transition tuples (prob, next_state, reward, done).
        env.nS is a number of states in the environment.
        env.nA is a number of actions in the environment.
        theta: We stop evaluation once our value function change is less than theta for all states.
        discount_factor: Gamma discount factor.
        
        Returns:
        A tuple (policy, V) of the optimal policy and the optimal value function.
        """
    
    def one_step_lookahead(state, V):
        """
            Helper function to calculate the value for all action in a given state.
            
            Args:
            state: The state to consider (int)
            V: The value to use as an estimator, Vector of length env.nS
            
            Returns:
            A vector of length env.nA containing the expected value of each action.
            """
        A = np.zeros(env.nA)
        D = np.zeros(env.nA)
        for a in range(env.nA):
            #print( state, a)
            #print("env P: ", state, a, env.P[state][a])
            for prob, next_state, reward, done in env.P[state][a]:
                A[a] += prob * (reward + discount_factor * V[next_state])
                D[a] = done
        return A,D
    
    V = np.zeros(env.nS)
    i =0
    K =env.K
    while True:
        # Stopping condition
        i = i + 1
        delta = 0
        # Update each state...
        for s in range(env.nS):
            #print("state: ",s)
            #print("Bary: ", Bary(s % K))
            if not_state(Bary(s % K)):
                continue
            #print("val s:",s)
            '''
            observability = ternary(s % K)
            changed = False
            for j in range(4):
                if observability[j] == '2':
                    observability = observability[:j] + "1" + observability[j+1:]
                    changed = True
            if changed:
                V[s] = V[int(s / K) * K + inv_ternary(observability)]
            else:
            '''
            # Do a one-step lookahead to find the best action
            A,D = one_step_lookahead(s, V)
            best_action_value = np.max(A)
            #best_action_index = np.argmax(A)
            # Calculate delta across all states seen so far
            delta = max(delta, np.abs(best_action_value - V[s]))
            # Update the value function
            V[s] = best_action_value
        # Check if we can stop
        if delta < theta:
            break

    # Create a deterministic policy using the optimal value function
    policy = np.zeros([env.nS, env.nA])
    for s in range(env.nS):
        # One step lookahead to find the best action for this state
        if not_state(Bary(s % K)):
            continue
        A,D = one_step_lookahead(s, V)
        best_action = np.argmax(A)
        # Always take the best action
        policy[s, best_action] = 1.0
        print("state:",s,"bestaction",best_action)
        
    for s in range(env.nS):
        # One step lookahead to find the best action for this state
        if not_state(Bary(s % K)):
            continue
        print("state:",s,"Value",V[s])
    
    return policy, V


'''
random_policy = np.ones([env.nS, env.nA]) / env.nA
v = policy_eval(random_policy, env)

print("Value Function:")
print(v)
print("")
'''

policy, v = policy_improvement(env)
print("Policy Probability Distribution (Policy Iteration):")
print(policy)
print("")

#print("Reshaped Grid Policy (0=up, 1=right, 2=down, 3=left):")
#print(np.reshape(np.argmax(policy, axis=1), env.shape))
#print("")

print("Value Function (Policy Iteration):")
print(v)
print("")

#print("Reshaped Grid Value Function:")
#print(v.reshape(env.shape))
#print("")


'''
policy, v = value_iteration(env)

print("Policy Probability Distribution (Value Iteraion):")
print(policy)
print("")

#print("Reshaped Grid Policy (0=up, 1=right, 2=down, 3=left):")
#print(np.reshape(np.argmax(policy, axis=1), env.shape))
#print("")

print("Value Function (Value Iteraion):")
print(v)
print("")

#print("Reshaped Grid Value Function:")
#print(v.reshape(env.shape))
#print("")
'''


# Test: Make sure the evaluated policy is what we expected
#expected_v = np.array([0, -14, -20, -22, -14, -18, -20, -20, -20, -20, -18, -14, -22, -20, -14, 0])
#np.testing.assert_array_almost_equal(v, expected_v, decimal=2)